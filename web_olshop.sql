-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.5.5-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping structure for table web_olshop.list_barang
CREATE TABLE IF NOT EXISTS `list_barang` (
  `barang_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `stok` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `berat` int(11) NOT NULL DEFAULT 1,
  `description` text NOT NULL,
  `foto` varchar(255) NOT NULL,
  PRIMARY KEY (`barang_id`),
  KEY `FK_list_barang_list_kategori_barang` (`kategori_id`),
  CONSTRAINT `FK_list_barang_list_kategori_barang` FOREIGN KEY (`kategori_id`) REFERENCES `list_kategori_barang` (`kategori_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table web_olshop.list_barang: ~15 rows (approximately)
/*!40000 ALTER TABLE `list_barang` DISABLE KEYS */;
INSERT INTO `list_barang` (`barang_id`, `kategori_id`, `nama`, `stok`, `harga`, `berat`, `description`, `foto`) VALUES
	(1, 1, 'Baju', 4, 100000, 200, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'gambar1.png'),
	(2, 1, 'Kemeja', 4, 80000, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'gambar2.jpeg'),
	(3, 8, 'Tas', 8, 100000, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'tas.jpg'),
	(4, 8, 'Tas Kece', 5, 50000, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'tas2.jpg'),
	(5, 7, 'Jam Murah', 5, 70000, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'jam.jpg'),
	(6, 7, 'Jam kece', 3, 120000, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'jam2.jpg'),
	(7, 6, 'Ponds', 7, 20000, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'ponds.jpg'),
	(8, 6, 'Ponds Murah', 3, 45000, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'ponds2.jpg'),
	(9, 6, 'Wardah', 2, 30000, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'wardah.jpg'),
	(10, 6, 'Wardah murah', 10, 50000, 100, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'wardah2.jpg'),
	(11, 1, 'Gamis', 0, 100000, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'gamis.jpg'),
	(12, 1, 'Gamis Murah', 7, 80000, 500, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'gamis2.jpg'),
	(13, 2, 'Hijab', 2, 12000, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'hijab.jpg'),
	(14, 2, 'Hijab Murah', 3, 20000, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'hijab2.jpg'),
	(15, 1, 'Baju Keren', 7, 100000, 250, 'lorem ipsum blajhsdjhsdfsdjf', 'gamis.jpg');
/*!40000 ALTER TABLE `list_barang` ENABLE KEYS */;

-- Dumping structure for table web_olshop.list_cart
CREATE TABLE IF NOT EXISTS `list_cart` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `barang_id` int(11) DEFAULT NULL,
  `jumlah_barang` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `FK__list_user` (`user_id`),
  KEY `FK__list_barang` (`barang_id`),
  CONSTRAINT `FK__list_barang` FOREIGN KEY (`barang_id`) REFERENCES `list_barang` (`barang_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK__list_user` FOREIGN KEY (`user_id`) REFERENCES `list_user` (`user_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- Dumping data for table web_olshop.list_cart: ~0 rows (approximately)
/*!40000 ALTER TABLE `list_cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_cart` ENABLE KEYS */;

-- Dumping structure for table web_olshop.list_checkout
CREATE TABLE IF NOT EXISTS `list_checkout` (
  `checkout_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `jumlah_barang` int(11) NOT NULL,
  PRIMARY KEY (`checkout_id`),
  KEY `FK_list_checkout_list_invoice` (`invoice_id`),
  KEY `FK_list_checkout_list_barang` (`barang_id`),
  CONSTRAINT `FK_list_checkout_list_barang` FOREIGN KEY (`barang_id`) REFERENCES `list_barang` (`barang_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_list_checkout_list_invoice` FOREIGN KEY (`invoice_id`) REFERENCES `list_invoice` (`invoice_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

-- Dumping data for table web_olshop.list_checkout: ~31 rows (approximately)
/*!40000 ALTER TABLE `list_checkout` DISABLE KEYS */;
INSERT INTO `list_checkout` (`checkout_id`, `invoice_id`, `barang_id`, `jumlah_barang`) VALUES
	(9, 10, 13, 1),
	(37, 37, 2, 10),
	(38, 38, 2, 5),
	(39, 39, 11, 1),
	(40, 40, 13, 1),
	(41, 41, 7, 1),
	(42, 42, 2, 1),
	(43, 42, 1, 5),
	(44, 42, 14, 3),
	(45, 43, 1, 3),
	(46, 43, 10, 2),
	(47, 43, 5, 3),
	(48, 44, 12, 3),
	(49, 44, 4, 4),
	(50, 44, 6, 3),
	(51, 45, 1, 1),
	(52, 45, 15, 5),
	(58, 53, 15, 2),
	(59, 54, 12, 1),
	(60, 55, 8, 2),
	(61, 56, 12, 2),
	(62, 56, 2, 1),
	(63, 57, 12, 1),
	(64, 57, 1, 1),
	(65, 58, 12, 2),
	(66, 58, 1, 1),
	(67, 59, 12, 1),
	(68, 59, 14, 1),
	(69, 60, 13, 1),
	(70, 60, 12, 1),
	(71, 61, 7, 2);
/*!40000 ALTER TABLE `list_checkout` ENABLE KEYS */;

-- Dumping structure for table web_olshop.list_gambar_barang
CREATE TABLE IF NOT EXISTS `list_gambar_barang` (
  `gambar_id` int(11) NOT NULL AUTO_INCREMENT,
  `barang_id` int(11) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  PRIMARY KEY (`gambar_id`),
  KEY `FK_list_gambar_barang_list_barang` (`barang_id`),
  CONSTRAINT `FK_list_gambar_barang_list_barang` FOREIGN KEY (`barang_id`) REFERENCES `list_barang` (`barang_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table web_olshop.list_gambar_barang: ~5 rows (approximately)
/*!40000 ALTER TABLE `list_gambar_barang` DISABLE KEYS */;
INSERT INTO `list_gambar_barang` (`gambar_id`, `barang_id`, `gambar`) VALUES
	(1, 1, 'gambar1.png'),
	(2, 1, 'gambar2.jpeg'),
	(3, 2, 'gambar1.png'),
	(5, 11, 'property-css-position-static-relative.png');
/*!40000 ALTER TABLE `list_gambar_barang` ENABLE KEYS */;

-- Dumping structure for table web_olshop.list_invoice
CREATE TABLE IF NOT EXISTS `list_invoice` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_number` varchar(50) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `nama_penerima` varchar(150) NOT NULL,
  `no_hp_penerima` varchar(20) DEFAULT NULL,
  `email_penerima` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `tanggal_jatuh_tempo` datetime DEFAULT NULL,
  `total_bayar` varchar(20) NOT NULL,
  `tanggal_bayar` datetime DEFAULT NULL,
  `invoice_status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`invoice_id`),
  KEY `FK_list_invoice_list_user` (`user_id`),
  KEY `FK_list_invoice_list_invoice_status` (`invoice_status_id`),
  CONSTRAINT `FK_list_invoice_list_invoice_status` FOREIGN KEY (`invoice_status_id`) REFERENCES `list_invoice_status` (`invoice_status_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_list_invoice_list_user` FOREIGN KEY (`user_id`) REFERENCES `list_user` (`user_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;


-- Dumping structure for table web_olshop.list_invoice_status
CREATE TABLE IF NOT EXISTS `list_invoice_status` (
  `invoice_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_status` varchar(50) NOT NULL,
  PRIMARY KEY (`invoice_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table web_olshop.list_invoice_status: ~2 rows (approximately)
/*!40000 ALTER TABLE `list_invoice_status` DISABLE KEYS */;
INSERT INTO `list_invoice_status` (`invoice_status_id`, `invoice_status`) VALUES
	(1, 'Belum Dibayar'),
	(2, 'Dibayar'),
	(3, 'Terkonfirmasi'),
	(4, 'Dibatalkan');
/*!40000 ALTER TABLE `list_invoice_status` ENABLE KEYS */;

-- Dumping structure for table web_olshop.list_kategori_barang
CREATE TABLE IF NOT EXISTS `list_kategori_barang` (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`kategori_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table web_olshop.list_kategori_barang: ~5 rows (approximately)
/*!40000 ALTER TABLE `list_kategori_barang` DISABLE KEYS */;
INSERT INTO `list_kategori_barang` (`kategori_id`, `nama`, `active`) VALUES
	(1, 'Pakaian Pria', 1),
	(2, 'Pakaian Wanita', 1),
	(6, 'Produk Kecantikan', 1),
	(7, 'Fashion Pria', 1),
	(8, 'Fashion Wanita', 1);
/*!40000 ALTER TABLE `list_kategori_barang` ENABLE KEYS */;

-- Dumping structure for table web_olshop.list_menu
CREATE TABLE IF NOT EXISTS `list_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table web_olshop.list_menu: ~3 rows (approximately)
/*!40000 ALTER TABLE `list_menu` DISABLE KEYS */;
INSERT INTO `list_menu` (`menu_id`, `menu`) VALUES
	(1, 'SUPER ADMIN'),
	(2, 'ADMIN'),
	(3, 'BARANG'),
	(4, 'TRANSAKSI');
/*!40000 ALTER TABLE `list_menu` ENABLE KEYS */;

-- Dumping structure for table web_olshop.list_pengiriman
CREATE TABLE IF NOT EXISTS `list_pengiriman` (
  `pengiriman_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `expedisi` varchar(50) NOT NULL,
  `alamat_tujuan` text NOT NULL,
  `harga_ongkir` varchar(50) NOT NULL,
  PRIMARY KEY (`pengiriman_id`),
  KEY `FK_list_pengiriman_list_invoice` (`invoice_id`),
  CONSTRAINT `FK_list_pengiriman_list_invoice` FOREIGN KEY (`invoice_id`) REFERENCES `list_invoice` (`invoice_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

-- Dumping structure for table web_olshop.list_role_user
CREATE TABLE IF NOT EXISTS `list_role_user` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(50) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table web_olshop.list_role_user: ~3 rows (approximately)
/*!40000 ALTER TABLE `list_role_user` DISABLE KEYS */;
INSERT INTO `list_role_user` (`role_id`, `role`) VALUES
	(1, 'Super Admin'),
	(2, 'Admin'),
	(3, 'User');
/*!40000 ALTER TABLE `list_role_user` ENABLE KEYS */;

-- Dumping structure for table web_olshop.list_sub_menu
CREATE TABLE IF NOT EXISTS `list_sub_menu` (
  `sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `header` varchar(50) NOT NULL,
  PRIMARY KEY (`sub_id`),
  KEY `FK_list_sub_menu_list_menu` (`menu_id`),
  CONSTRAINT `FK_list_sub_menu_list_menu` FOREIGN KEY (`menu_id`) REFERENCES `list_menu` (`menu_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table web_olshop.list_sub_menu: ~8 rows (approximately)
/*!40000 ALTER TABLE `list_sub_menu` DISABLE KEYS */;
INSERT INTO `list_sub_menu` (`sub_id`, `menu_id`, `title`, `icon`, `link`, `header`) VALUES
	(1, 1, 'Dashboard', 'nav-icon fas fa-tachometer-alt', 'dashboard', 'Dashboard'),
	(2, 1, 'Menu', 'nav-icon fas fa-list', 'menu', 'Menu Management'),
	(3, 1, 'Sub Menu', 'nav-icon fas fa-chevron-circle-down', 'sub_menu', 'Sub Menu Management'),
	(5, 1, 'Admin', 'nav-icon fas fa-user', 'admin', 'Admin Management'),
	(6, 2, 'User', 'nav-icon fas fa-users', 'user', 'User Management'),
	(7, 3, 'Kategori', 'nav-icon fas fa-th-list', 'kategori_barang', 'Kategori Barang Management'),
	(8, 3, 'Barang', 'nav-icon fas fa-boxes', 'barang', 'Barang Management'),
	(9, 2, 'Settings', 'nav-icon fas fa-cog', 'settings', 'Setting Toko'),
	(11, 4, 'Pesanan Masuk', 'nav-icon fas fa-shopping-cart', 'pesanan_masuk', 'Pesanan Masuk Management');
/*!40000 ALTER TABLE `list_sub_menu` ENABLE KEYS */;

-- Dumping structure for table web_olshop.list_user
CREATE TABLE IF NOT EXISTS `list_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(20) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `FK_list_user_list_role_user` (`role_id`),
  CONSTRAINT `FK_list_user_list_role_user` FOREIGN KEY (`role_id`) REFERENCES `list_role_user` (`role_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table web_olshop.list_user: ~1 rows (approximately)
/*!40000 ALTER TABLE `list_user` DISABLE KEYS */;
INSERT INTO `list_user` (`user_id`, `nama`, `email`, `phone`, `username`, `password`, `role_id`) VALUES
	(8, 'Mohammad Ricky Sanjaya, S. Kom.', 'rickysanjaya411@gmail.com', '0', 'sanjaya', '$2y$10$1d3qfDb7jssd3s8WS2XdwODrg1WIoq7APed/noges4pXqoNIY/vyS', 1),
	(10, 'Mohammad Ricky Sanjaya', 'rickysanjaya41@gmail.com', '081213490755', 'sanjaya411', '$2y$10$j6/hDE2EDF9m8lcrYaUOc.DcQFEDYPd3QC.eBos4c/IZv3WREvXL.', 3);
/*!40000 ALTER TABLE `list_user` ENABLE KEYS */;

-- Dumping structure for table web_olshop.rel_access
CREATE TABLE IF NOT EXISTS `rel_access` (
  `access_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`access_id`),
  KEY `FK__list_role_user` (`role_id`),
  KEY `FK__list_menu` (`menu_id`),
  CONSTRAINT `FK__list_menu` FOREIGN KEY (`menu_id`) REFERENCES `list_menu` (`menu_id`) ON UPDATE CASCADE,
  CONSTRAINT `FK__list_role_user` FOREIGN KEY (`role_id`) REFERENCES `list_role_user` (`role_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table web_olshop.rel_access: ~7 rows (approximately)
/*!40000 ALTER TABLE `rel_access` DISABLE KEYS */;
INSERT INTO `rel_access` (`access_id`, `role_id`, `menu_id`) VALUES
	(1, 1, 1),
	(2, 1, 2),
	(3, 2, 2),
	(4, 1, 3),
	(5, 2, 3),
	(6, 1, 4),
	(7, 2, 4);
/*!40000 ALTER TABLE `rel_access` ENABLE KEYS */;

-- Dumping structure for table web_olshop.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_toko` varchar(50) DEFAULT NULL,
  `provinsi` int(11) DEFAULT NULL,
  `kota` int(11) DEFAULT NULL,
  `alamat_toko` text DEFAULT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table web_olshop.settings: ~0 rows (approximately)
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `nama_toko`, `provinsi`, `kota`, `alamat_toko`, `no_hp`, `email`) VALUES
	(1, 'SansShop', 9, 54, 'Cikarang Barat, Bekasi', '0815747844', 'rickysanjaya@mail.com');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

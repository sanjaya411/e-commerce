<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ongkir extends CI_Controller {
  private $api_key = "34316eb45f72c89acc7dd76bc813e883";

  public function provinsi()
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "key: $this->api_key"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      // echo $response;
      $result = json_decode($response, TRUE);
      $data_provinsi = $result["rajaongkir"]["results"];
      echo "<option disabled selected>-- Pilih Provinsi --</option>";
      foreach ($data_provinsi as $item) {
        echo "<option value='".$item["province_id"]."'>".$item["province"]."</option>";
      }
      // echo "<pre>";
      // print_r($result["rajaongkir"]["results"]);
      // echo "</pre>";
    }
  }

  public function kota()
  {
    $provinsi_id = html_escape($this->input->post("provinsi_id"));
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.rajaongkir.com/starter/city?province=".$provinsi_id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "key: $this->api_key"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      // echo $response;
      $result = json_decode($response, TRUE);
      $data_kota = $result["rajaongkir"]["results"];
      echo "<option disabled selected>-- Pilih Kota/Kabupaten --</option>";
      foreach ($data_kota as $item) {
        echo "<option value='" . $item["city_id"] . "'>" . $item["type"] ."  ". $item["city_name"] . "</option>";
      }
      // echo "<pre>";
      // print_r($data_kota);
      // echo "</pre>";
    }
  }

  public function expedisi()
  {
    echo "<option selected disabled>-- Pilih Expedisi --</option>";
    echo "<option value='jne'>JNE</option>";
    echo "<option value='tiki'>Tiki</option>";
    echo "<option value='pos'>POS Indonesia</option>";
  }

  public function services()
  {
    $settings = $this->M_data->get_data("settings")->data->row();
    $origin = $settings->kota;
    $input = (object)html_escape($this->input->post());
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "origin=$origin&destination=$input->destination_id&weight=$input->berat&courier=$input->expedisi",
      CURLOPT_HTTPHEADER => array(
        "content-type: application/x-www-form-urlencoded",
        "key: $this->api_key"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      $hasil = new stdClass();
      $result = json_decode($response, TRUE);
      // echo "<pre>";
      // print_r($result["rajaongkir"]["results"][0]);
      // echo "</pre>";
      $service = $result["rajaongkir"]["results"][0]["costs"];
      $expedisi = $result["rajaongkir"]["results"][0]["name"];
      $hasil->option = "<option disabled selected>-- Pilih Layanan --</option>";
      $hasil->expedisi = [];
      foreach ($service as $item) {
        $item["cost"][0]["etd"] = str_replace("HARI", "", $item["cost"][0]["etd"]);
        array_push($hasil->expedisi, $item["service"]." - ".$item["description"]);
        $hasil->option .= "<option value='" . $expedisi . " - " . $item["service"] . "Rp. z" . $item["cost"][0]["value"] . "'>".$item["service"]." - Rp. ".number_format($item["cost"][0]["value"],"0",",",".")." ".$item["cost"][0]["etd"]." Hari</option>";
      }
      echo json_encode($hasil);
    }
  }
}
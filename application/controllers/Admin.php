<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->user_login->protect_login();
  }

  public function view_user_management()
  {
    $check = $this->M_data->get_user();
    if ($check->success === TRUE) {
      $data = [
        "user_info" => $this->M_data->user_info()->data,
        "list_menu" => $this->M_data->get_menu($this->session->userdata("role_id")),
        "title" => "Admin - User Management",
        "title_admin" => "User Management",
        "list_user" => $check->data,
        "isi" => "admin/v_user"
      ];
      $this->load->view('layout/v_wrapper', $data, FALSE);
    } else {
      echo "Query failed!";
    }
  }

  public function process_user_add()
  {
    $this->form_validation->set_rules('nama', 'User', 'required');
    $this->form_validation->set_rules('Emali', 'User', 'required|valid_email');
    $this->form_validation->set_rules('username', 'User', 'required');
    $this->form_validation->set_rules('password', 'User', 'required');

    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal!", "Isi data dengan benar!")</script>');
      redirect("user");
    } else {
      $input = (object)html_escape($this->db->escape_str($this->input->post()));
      $data = [
        "nama" => $input->nama,
        "email" => $input->email,
        "username" => $input->username,
        "password" => password_hash($input->password, PASSWORD_DEFAULT),
        "role_id" => 3
      ];
      $check = $this->M_data->insert_data($data, "list_user");
      if ($check->success === TRUE) {
        $this->session->set_flashdata('pesan', '<script>sweet("success", "Sukses!", "User berhasil ditambah!")</script>');
        redirect("user");
      } else {
        $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal!", "User gagal ditambah!")</script>');
        redirect("user");
      }
    }
  }

  public function view_user_edit()
  {
    $response = new stdClass();
    $user_id = (int)html_escape($this->db->escape_str($this->input->get("id")));
    $check = $this->M_data->get_user_detail($user_id);
    if ($check->success === TRUE) {
      $response->success = TRUE;
      $response->data = $check->data;
    } else {
      $response->success = FALSE;
    }
    echo json_encode($response);
  }

  public function process_user_edit()
  {
    $this->form_validation->set_rules('user_id', 'User', 'required|numeric');
    $this->form_validation->set_rules('nama', 'User', 'required');
    $this->form_validation->set_rules('email', 'User', 'required|valid_email');
    $this->form_validation->set_rules('username', 'User', 'required');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal!", "Input data dengan benar!")</script>');
      redirect("user");
    } else {
      $input = (object)html_escape($this->db->escape_str($this->input->post()));
      $data = [
        "email" => $input->email,
        "username" => $input->username,
        "nama" => $input->nama
      ];
      if ($input->password != "") {
        $data["password"] = password_hash($input->password, PASSWORD_DEFAULT);
      }
      $where = ["user_id" => $input->user_id];
      $check = $this->M_data->update_data($data, $where, "list_user");
      if ($check->success === TRUE) {
        $this->session->set_flashdata('pesan', '<script>sweet("success", "Sukses!", "User berhasil diupdate!")</script>');
        redirect("user");
      } else {
        $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal!", "User gagal diupdate!")</script>');
        redirect("user");
      }
    }
  }

  public function process_user_delete($id)
  {
    $user_id = (int)html_escape($this->db->escape_str($id));
    $where = ["user_id" => $user_id];
    $check = $this->M_data->delete_data($where, "list_user");
    if ($check->success === TRUE) {
      $this->session->set_flashdata('pesan', '<script>sweet("success", "Sukses!", "User berhasil dihapus!")</script>');
      redirect("user");
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal!", "User gagal dihapus!")</script>');
      redirect("user");
    }
  }
}
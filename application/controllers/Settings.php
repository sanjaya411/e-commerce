<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

  public function index()
  {
    $data = [
      "user_info" => $this->M_data->user_info()->data,
      "list_menu" => $this->M_data->get_menu($this->session->userdata("role_id")),
      "title" => "Settings - Dashboard",
      "title_admin" => "Setting Toko",
      "isi" => "admin/v_settings",
      "setting" => $this->M_data->edit_data(["id" => 1], "settings")->data
    ];
    $this->load->view('layout/v_wrapper', $data);
  }

  public function validate_settings_edit()
  {
    $this->form_validation->set_rules('nama_toko', 'Settings', 'required');
    $this->form_validation->set_rules('provinsi_id', 'Settings', 'required');
    $this->form_validation->set_rules('kota_id', 'Settings', 'required');
    $this->form_validation->set_rules('alamat_toko', 'Settings', 'required');
    $this->form_validation->set_rules('no_hp', 'Settings', 'required|numeric');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "isi data dengan benar!")</script>');
      redirect("settings");
    } else {
      $this->process_settings_edit();
    }
  }

  private function process_settings_edit()
  {
    $input = (object)html_escape($this->input->post());
    $data = [
      "nama_toko" => $this->db->escape_str($input->nama_toko),
      "provinsi" => $this->db->escape_str($input->provinsi_id),
      "kota" => $this->db->escape_str($input->kota_id),
      "alamat_toko" => $this->db->escape_str($input->alamat_toko),
      "no_hp" => $this->db->escape_str($input->no_hp),
    ];
    $check = $this->M_data->update_data($data, ["id" => 1], "settings");
    if ($check->success === TRUE) {
      $this->session->set_flashdata('pesan', '<script>sweet("success", "Sukses", "Update data berhasil!")</script>');
      redirect("settings");
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Query failed!")</script>');
      redirect("settings");
    }
  }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->user_login->protect_login();
  }

  public function view_kategori_barang()
  {
    $check = $this->M_data->get_data("list_kategori_barang");
    if ($check->success === TRUE) {
      $data = [
        "user_info" => $this->M_data->user_info()->data,
        "list_menu" => $this->M_data->get_menu($this->session->userdata("role_id")),
        "title" => "Admin - Kategori Barang",
        "title_admin" => "Kategori Barang Management",
        "isi" => "admin/v_ketegori_barang",
        "list_kategori" => $check->data->result()
      ];
      $this->load->view('layout/v_wrapper', $data, FALSE);
    } else {
      redirect("dashboard");
    }
  }

  public function validate_kategori_add()
  {
    $this->form_validation->set_rules('nama', 'Kategori', 'required');
    $this->form_validation->set_rules('active', 'Kategori', 'required|numeric');
    
    if ($this->form_validation->run() == TRUE) {
      $this->process_kategori_barang_add();
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Isi data dengan benar & lengkap!")</script>');
      redirect("kategori_barang");      
    }
  }

  private function process_kategori_barang_add()
  {
    $input = (object)html_escape($this->db->escape_str($this->input->post()));
    $data = [
      "nama" => $input->nama,
      "active" => $input->active
    ];
    $check = $this->M_data->insert_data($data, "list_kategori_barang");
    if ($check->success === TRUE) {
      $this->session->set_flashdata('pesan', '<script>sweet("success", "Suksesl", "Kategori barang berhasil ditambah!")</script>');
      redirect("kategori_barang"); 
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "gagall", "Kategori barang gagal ditambah!")</script>');
      redirect("kategori_barang"); 
    }
  }

  public function view_kategori_edit()
  {
    $response = new stdClass();
    $kategori = (int)html_escape($this->db->escape_str($this->input->get("id")));
    $check = $this->M_data->edit_data(["kategori_id" => $kategori], "list_kategori_barang");
    if ($check->success === TRUE) {
      $response->success = 200;
      $response->data = $check->data;
    } else {
      $response->success = 201;
    }
    echo json_encode($response);
  }

  public function validate_kategori_edit()
  {
    $this->form_validation->set_rules('nama', 'Kategori', 'required');
    $this->form_validation->set_rules('active', 'Kategori', 'required|numeric');
    $this->form_validation->set_rules('kategori_id', 'Kategori', 'required|numeric');

    if ($this->form_validation->run() == TRUE) {
      $this->process_kategori_barang_edit();
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Isi data dengan benar & lengkap!")</script>');
      redirect("kategori_barang");
    }
  }

  private function process_kategori_barang_edit()
  {
    $input = (object)html_escape($this->db->escape_str($this->input->post()));
    $data = [
      "nama" => $input->nama,
      "active" => $input->active
    ];
    $check = $this->M_data->update_data($data, ["kategori_id" => $input->kategori_id], "list_kategori_barang");
    if ($check->success === TRUE) {
      $this->session->set_flashdata('pesan', '<script>sweet("success", "Suksesl", "Kategori barang berhasil diupdate!")</script>');
      redirect("kategori_barang");
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "gagall", "Kategori barang gagal diupdate!")</script>');
      redirect("kategori_barang");
    }
  }

  public function process_kategori_delete($id)
  {
    $kategori_id = (int)html_escape($this->db->escape_str($id));
    $check = $this->M_data->delete_data(["kategori_id" => $kategori_id], "list_kategori_barang");
    if ($check->success === TRUE) {
      $this->session->set_flashdata('pesan', '<script>sweet("success", "Suksesl", "Kategori barang berhasil dihapus!")</script>');
      redirect("kategori_barang");
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "gagall", "Kategori barang gagal dihapus!")</script>');
      redirect("kategori_barang");
    }
  }

  public function view_barang_management()
  {
    $check = $this->M_data->get_barang_list();
    if ($check->success === TRUE) {
      $data = [
        "user_info" => $this->M_data->user_info()->data,
        "list_menu" => $this->M_data->get_menu($this->session->userdata("role_id")),
        "title" => "Admin - Barang",
        "title_admin" => "Barang Management",
        "list_barang" => $check->data,
        "list_kategori" => $this->M_data->get_data("list_kategori_barang")->data->result(),
        "isi" => "admin/v_barang"
      ];
      $this->load->view('layout/v_wrapper', $data, FALSE);
    } else {
      redirect("dashboard");
    }
  }

  public function validate_barang_add()
  {
    $this->form_validation->set_rules('kategori_id', 'Barang', 'required');
    $this->form_validation->set_rules('nama', 'Barang', 'required');
    $this->form_validation->set_rules('berat', 'Barang', 'required');
    $this->form_validation->set_rules('stok', 'Barang', 'required');
    $this->form_validation->set_rules('harga', 'Barang', 'required');
    $this->form_validation->set_rules('description', 'Barang', 'required');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Data yang anda masukan salah/kurang lengkap!")</script>');
      redirect("barang");
    } else {
      $this->process_barang_add();
    }
  }

  private function process_barang_add()
  {
    $input = (object)html_escape($this->db->escape_str($this->input->post()));
    $config['upload_path']          = './assets/img/';
    $config['allowed_types']        = 'jpg|png|jpeg';
    $config['max_size']             = 5048;
    $config['max_width']            = 5048;
    $config['max_height']           = 5048;
    $this->upload->initialize($config);
    if (!$this->upload->do_upload("foto")) {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Foto gagal diupload karena '.$this->upload->display_errors().'!")</script>');
      redirect("barang");
    } else {
      $foto = str_replace(" ", "_", $_FILES["foto"]["name"]);
      $data = [
        "kategori_id" => $input->kategori_id,
        "nama" => $input->nama,
        "stok" => $input->stok,
        "berat" => $input->berat,
        "harga" => $input->harga,
        "description" => $input->description,
        "foto" => $foto
      ];
      $check = $this->M_data->insert_data($data, "list_barang");
      if ($check->success === TRUE) {
        $this->session->set_flashdata('pesan', '<script>sweet("success", "Sukses", "Data barang telah ditambahkan!")</script>');
        redirect("barang");
      } else {
        unlink(base_url("assets/img/".$_FILES["foto"]["name"]));
        $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Query failed!")</script>');
      redirect("barang");
      }
    }
  }

  public function view_barang_edit()
  {
    $response = new stdClass();
    $barang_id = (int)html_escape($this->db->escape_str($this->input->get("id")));
    $check = $this->M_data->get_barang_detail($barang_id);
    if ($check->success === TRUE) {
      $response->success = 200;
      $response->data = $check->data;
    } else {
      $response->success = 201;
    }
    echo json_encode($response);
  }

  public function validate_barang_edit()
  {
    $this->form_validation->set_rules('kategori_id', 'Barang', 'required');
    $this->form_validation->set_rules('nama', 'Barang', 'required');
    $this->form_validation->set_rules('stok', 'Barang', 'required');
    $this->form_validation->set_rules('berat', 'Barang', 'required');
    $this->form_validation->set_rules('harga', 'Barang', 'required');
    $this->form_validation->set_rules('description', 'Barang', 'required');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Data yang anda masukan salah/kurang lengkap!")</script>');
      redirect("barang");
    } else {
      $this->process_barang_edit();
    }
  }

  private function process_barang_edit()
  {
    $input = (object)html_escape($this->db->escape_str($this->input->post()));
    $error = FALSE;
    $data = [
      "kategori_id" => $input->kategori_id,
      "nama" => $input->nama,
      "stok" => $input->stok,
      "berat" => $input->berat,
      "harga" => $input->harga,
      "description" => $input->description
    ];
    if ($_FILES["foto"]["name"] != "") {
      $config['upload_path']          = './assets/img/';
      $config['allowed_types']        = 'jpg|png|jpeg';
      $config['max_size']             = 5048;
      $config['max_width']            = 2048;
      $config['max_height']           = 2048;
      $this->upload->initialize($config);
      if (!$this->upload->do_upload("foto")) {
        $error = TRUE;
      } else {
        $data["foto"] = str_replace(" ", "_", $_FILES["foto"]["name"]);
        $data_sebelumnya = $this->M_data->get_barang_detail($input->barang_id);
        $gambar_sebelumnya = $data_sebelumnya->data->foto;
        unlink(PUBPATH . "assets/img/" . $gambar_sebelumnya);
      }
    }
    if ($error === FALSE) {
      $check = $this->M_data->update_data($data, ["barang_id" => $input->barang_id], "list_barang");
      if ($check->success === TRUE) {
        $this->session->set_flashdata('pesan', '<script>sweet("success", "Sukses", "Data barang berhasil diupdate!")</script>');
        redirect("barang");
      } else {
        $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Query failed!")</script>');
        redirect("barang");
      }
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Gagal Upload gambar karena '.$this->upload->display_errors().'")</script>');
      redirect("barang");
    }
  }

  public function process_barang_delete($id)
  {
    $error = FALSE;
    $message = "";
    $data_gambar = [];
    $gambar_cover = "";
    $barang_id = (int) html_escape($this->db->escape_str($id));
    $this->db->trans_begin();
    $query = $this->M_data->edit_data(["barang_id" => $barang_id], "list_gambar_barang");
    if ($query->query) {
      if ($query->total > 1) {
        foreach ($query->data as $item) {
          array_push($data_gambar, $item->gambar);
        }
      } else {
        array_push($data_gambar, $query->data->gambar);
      }
      $query2 = $this->M_data->delete_data(["barang_id" => $barang_id], "list_gambar_barang");
      if ($query2->success === TRUE) {
        $query3 = $this->M_data->edit_data(["barang_id" => $barang_id], "list_barang");
        if ($query3->success === TRUE) {
          $gambar_cover = $query3->data->foto;
          $query4 = $this->M_data->delete_data(["barang_id" => $barang_id], "list_barang");
          if ($query4->success !== TRUE) {
            $error = TRUE;
            $message = "Query 4 failed";
          }
        } else {
          $error = TRUE;
          $message = "Query 3 failed";
        }
      } else {
        $error = TRUE;
        $message = "Query 2 failed!";
      }
    } else {
      $error = TRUE;
      $message = "Query 1 failed!";
    }
    if ($error === FALSE && $this->db->trans_status() === TRUE) {
      for ($i = 0; $i < count($data_gambar); $i++) {
        unlink(PUBPATH . "assets/img_barang/" . $data_gambar[$i]);
      }
      unlink(PUBPATH . "assets/img/" . $gambar_cover);
      $this->db->trans_commit();
      $this->session->set_flashdata('pesan', '<script>sweet("success", "Sukses", "Data barang berhasil dihapus!")</script>');
      redirect("barang");
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "'.$message.'")</script>');
      redirect("barang");
    }
  }

  public function view_barang_detail($id)
  {
    $barang_id = (int)html_escape($this->db->escape_str($id));
    $check = $this->M_data->get_barang_detail($barang_id);
    if ($check->success === TRUE) {
      $data = [
        "user_info" => $this->M_data->user_info()->data,
        "list_menu" => $this->M_data->get_menu($this->session->userdata("role_id")),
        "title" => "Admin - Barang",
        "title_admin" => "Barang Management",
        "barang" => $check->data,
        "list_kategori" => $this->M_data->get_data("list_kategori_barang")->data->result(),
        "isi" => "admin/v_gambar_barang_add"
      ];
      $this->load->view('layout/v_wrapper', $data, FALSE);
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Query failed!")</script>');
      redirect("barang");
    }
  }

  public function view_gambar_barang()
  {
    $response = new stdClass();
    $response->html = "";
    $barang_id = (int)html_escape($this->db->escape_str($this->input->get("id")));
    $check = $this->M_data->edit_data(["barang_id" => $barang_id], "list_gambar_barang");
    if ($check->success === TRUE) {
      $response->success = 200;
      if($check->total > 0) {
        foreach ($check->query->result() as $item) {
          $response->html .= "<div class='col-md-4'>";
          $response->html .= "<div class='card shadow'>";
          $response->html .= "<img class='card-img-top' style='width: 100%; height: 250px;' src='" . base_url("assets/img_barang/" . $item->gambar) . "'>";
          $response->html .= "<div class='card-footer p-0 bg-white'>";
          $response->html .= "<p class='btn btn-outline-danger mb-0' style='width: 100%; height: 100%;' onclick=\"deletePrompt('" . base_url("delete_gambar?id=$item->gambar_id&barang_id=$barang_id") . "', '#loading')\">Hapus</p>";
          $response->html .= "</div>";
          $response->html .= "</div>";
          $response->html .= "</div>";
        }
      } else {
        $response->html .= "<h5>Belum ada list gambar</h5>";
      }
    } else {
      $response->success = 201;
    }
    echo json_encode($response);
  }

  public function process_gambar_barang_add()
  {
    $this->load->library("fungsi");
    $response = new stdClass();
    $gambar = $_FILES["file"]["name"];
    $barang_id = (int)html_escape($this->db->escape_str($this->input->get("id")));
    if ($gambar != "") {
      $config['upload_path']          = './assets/img_barang/';
      $config['allowed_types']        = 'jpg|png|jpeg';
      $config['max_size']             = 5048;
      $config['max_width']            = 5048;
      $config['max_height']           = 5048;
      $this->upload->initialize($config);
      if (!$this->upload->do_upload("file")) {
        $response->success = 201;
        $response->message = $this->upload->display_errors();
      } else {
        $gambar = str_replace(" ", "_", $_FILES["file"]["name"]);
        $data = [
          "barang_id" => $barang_id,
          "gambar" => $gambar
        ];
        $check = $this->M_data->insert_data($data, "list_gambar_barang");
        if ($check->success === TRUE) {
          $response->success = 200;
        } else {
          $response->sucess = 202;
          $response->message = "Query Error";
        }
      }
    } else {
      $response->success = 203;
      $response->message = "No data";
    }
    echo json_encode($response);
  }

  public function process_gambar_barang_delete()
  {
    $gambar_id = (int)html_escape($this->db->escape_str($this->input->get("id")));
    $barang_id = (int)html_escape($this->db->escape_str($this->input->get("barang_id")));
    $data_sebelumnya = $this->M_data->edit_data(["gambar_id" => $gambar_id], "list_gambar_barang");
    $foto = $data_sebelumnya->data->gambar;
    $check = $this->M_data->delete_data(["gambar_id" => $gambar_id], "list_gambar_barang");
    if ($check->success === TRUE) {
      unlink(PUBPATH . "assets/img_barang/" . $foto);
      $this->session->set_flashdata('pesan', '<script>sweet("success", "Sukses", "Gambar berhasil dihapus!")</script>');
      redirect("detail_barang/$barang_id");
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Query failed!")</script>');
      redirect("detail_barang/$barang_id");
    }
  }
}
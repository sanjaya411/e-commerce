<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    if ($this->session->login === TRUE) {
      $this->cart->destroy();
    }
    date_default_timezone_set("Asia/Jakarta");
    $this->load->library("invoice");
    $this->invoice->check_invoice();
    // $this->cart->destroy();
  }

  public function index()
  {
    $data = [
      "title" => "Home",
      "isi" => "v_home",
      "list_barang" => $this->M_data->get_barang_list_home()->data,
      "total_barang" => $this->M_data->get_barang_list_home()->total,
      "kategori" => $this->M_data->edit_data(["active" => 1], "list_kategori_barang")->data
      //   "user" => $this->M_data->user_info()->data
    ];
    // echo "<pre>";
    // print_r($data);
    // echo ">/pre>";
    // var_dump($this->M_data->edit_data(["active" => 1],"list_kategori_barang")->data);
    $this->load->view("layout/v_wrapper_fe", $data);
  }

  public function kategori()
  {
    $page = (int)html_escape($this->db->escape_str($this->input->get("page")));
    $id = decrypt_url(html_escape($this->input->get("id")));
    $check = $this->M_data->get_barang_kategori($page, $id);
    $pagination = $this->M_data->get_pagination_barang_kategori($id);
    if ($check !== FALSE && $page <= $pagination) {
      $nama_kategori = $this->M_data->edit_data(["kategori_id" => $id], "list_kategori_barang");
      $data = [
        "user" => $this->M_data->get_user_detail()->data,
        "kategori_id" => $id,
        "title" => "List Barang - " . $nama_kategori->data->nama,
        "nama_kategori" => $nama_kategori->data->nama,
        "isi" => "user/barang_kategori",
        "list_barang" => $check,
        "page" => $page,
        "pagination" => $pagination,
        "kategori" => $this->M_data->edit_data(["active" => 1], "list_kategori_barang")->data
      ];
      $this->load->view("layout/v_wrapper_fe", $data, FALSE);
    } else {
      redirect("index");
    }
  }

  public function view_detail_barang()
  {
    $id = decrypt_url(html_escape($this->input->get("id")));
    $check = $this->M_data->get_barang_detail($id);
    if ($check->success === TRUE) {
      $data = [
        "user" => $this->M_data->get_user_detail()->data,
        "title" => "Barang - " . $check->data->nama,
        "isi" => "user/v_detail_barang",
        "barang" => $check->data,
        "list_foto" => $this->M_data->get_foto_barang($id),
        "kategori" => $this->M_data->edit_data(["active" => 1], "list_kategori_barang")->data
      ];
      $this->load->view("layout/v_wrapper_fe", $data, FALSE);
    } else {
      redirect("index");
    }
  }

  public function all_barang()
  {
    $page = (int)html_escape($this->input->get("page"));
    $check = $this->M_data->get_all_barang($page);
    $pagination = $this->M_data->get_pagination_barang();
    if ($check->success === TRUE && $page <= $pagination) {
      $data = [
        "user" => $this->M_data->get_user_detail()->data,
        "title" => "Semua Barang",
        "isi" => "user/v_all_barang",
        "list_barang" => $check->data,
        "pagination" => $pagination,
        "page" => $page,
        "kategori" => $this->M_data->edit_data(["active" => 1], "list_kategori_barang")->data
      ];
      $this->load->view("layout/v_wrapper_fe", $data, FALSE);
    } else {
      redirect("index");
    }
  }

  public function add_cart()
  {
    $response = new stdClass();
    $id = decrypt_url(html_escape($this->input->get("id")));
    $check = $this->M_data->edit_data(["barang_id" => $id], "list_barang");
    if ($check->success === TRUE) {
      if ($check->data->stok > 0) {
        if ($this->session->userdata("login") === TRUE) {
          $cek_cart = $this->M_data->cek_cart($id);
          if ($cek_cart->success === TRUE) {
            $add_cart = FALSE;
            $list = $cek_cart->data->jumlah_barang;
            if ($check->data->stok > $list) {
              if ($cek_cart->total == 0) {
                $data2 = [
                  "user_id" => $this->session->user_id,
                  "barang_id" => $id,
                  "jumlah_barang" => 1,
                  "created_date" => date("Y-m-d H:i:s")
                ];
                $add_cart = $this->M_data->insert_data($data2, "list_cart");
              } else {
                $data2 = [
                  "jumlah_barang" => $cek_cart->data->jumlah_barang + 1
                ];
                $where = [
                  "user_id" => $this->session->user_id,
                  "barang_id" => $id
                ];
                $add_cart = $this->M_data->update_data($data2, $where, "list_cart");
              }
              if ($add_cart->success === TRUE) {
                $this->session->set_flashdata('pesan', '<script>sweet("success", "Sukses", "Barang berhasil dimasukan ke keranjang!")</script>');
                $response->success = TRUE;
                $response->stok = $check->data->stok;
                $response->list = $list;
              } else {
                $response->success = FALSE;
                $response->message = $cek_cart->total;
              }
            } else {
              $response->success = FALSE;
              $response->message = "Maaf barang yang anda pesan telah habis!";
            }
          } else {
            $response->success = FALSE;
            $response->message = "Failed get info cart";
          }
        } else {
          $cart = $this->cart->contents();
          $accept_cart = TRUE;
          foreach ($cart as $item) {
            if ($item["id"] === $id) {
              $response->halo = "Masuk";
              if ($item["qty"] >= $check->data->stok) {
                $accept_cart = FALSE;
                $response->debug = "Masuk";
              }
            }
          }

          if ($accept_cart === TRUE) {
            $data2 = [
              "id" => $id,
              "qty" => 1,
              "price" => $check->data->harga,
              "name" => $check->data->nama,
              "foto" => $check->data->foto,
              "stok" => $check->data->stok,
              "berat" => $check->data->berat
            ];
            $insert_cart = $this->cart->insert($data2);
            if ($insert_cart) {
              $this->session->set_flashdata('pesan', '<script>sweet("success", "Sukses", "Barang berhasil dimasukan ke keranjang!")</script>');
              $response->success = TRUE;
              $response->debug = "Masuk";
            } else {
              $response->success = FALSE;
              $response->message = "Failed insert to cart!";
            }
          } else {
            $response->success = FALSE;
            $response->message = "Maaf barang yang anda pesan telah habis!";
          }
        }
      } else {
        $response->success = FALSE;
        $response->message = "Stok barang tidak mencukupi!";
      }
    } else {
      $response->success = FALSE;
      $response->message = "Query failed!";
    }
    echo json_encode($response);
  }

  public function view_cart()
  {
    $html = "";
    if ($this->session->login !== TRUE) {
      $total_item = $this->cart->total_items();
      $total_harga = number_format($this->cart->total(), "0", ",", ".");
      echo json_encode(["total" => $total_item, "total_harga" => $total_harga]);
    } else {
      $get_cart = $this->M_data->get_cart_user();
      $total_item = 0;
      $total_harga = 0;
      if ($get_cart->success === TRUE) {
        foreach ($get_cart->data as $item) {
          $total_harga += $item->jumlah_barang * $item->harga;
          $total_item += $item->jumlah_barang;
        }
      } else {
        $html += $get_cart->debug;
      }
      echo json_encode(["total" => $total_item, "total_harga" => number_format($total_harga, "0", ",", ".")]);
    }
  }

  public function view_keranjang()
  {
    $data = [
      "user" => $this->M_data->get_user_detail()->data,
      "title" => "Keranjang Belanja",
      "isi" => "user/v_keranjang",
      "kategori" => $this->M_data->edit_data(["active" => 1], "list_kategori_barang")->data,
      "list_cart" => $this->M_data->get_cart_user()->data
    ];
    $this->load->view("layout/v_wrapper_fe", $data, FALSE);
  }

  public function update_cart()
  {
    $response = new stdClass();
    $id = decrypt_url(html_escape($this->input->get("id")));
    $qty = html_escape($this->input->post("qty"));
    $row_id = html_escape($this->input->post("row_id"));
    $qty1 = html_escape($this->input->post("qty1"));
    $check = $this->M_data->edit_data(["barang_id" => $id], "list_barang");
    if ($check->success === TRUE) {
      if ($check->data->stok >= $qty) {
        if ($this->session->userdata("login") === TRUE) {
          $cek_cart = $this->M_data->cek_cart($id);
          if ($cek_cart->success === TRUE) {
            $data2 = [
              "jumlah_barang" => $qty
            ];
            $where = [
              "user_id" => $this->session->user_id,
              "barang_id" => $id
            ];
            $update_cart = $this->M_data->update_data($data2, $where, "list_cart");
            if ($update_cart) {
              $get_cart = $this->M_data->get_cart_user();
              $total_harga = 0;
              foreach ($get_cart->data as $item) {
                $berat = $item->jumlah_barang * $item->berat;
                $response->total_berat += $berat;
                $harga = $item->jumlah_barang * $item->harga;
                $total_harga += $harga;
              }
              $harga = $qty * $check->data->harga;
              $response->harga = "Rp. " . number_format($harga, "0", ",", ".") . " ,-";
              $response->total_harga = "Rp. " . number_format($total_harga, "0", ",", ".") . " ,-";
              $response->success = TRUE;
              $response->berat = $qty * $check->data->berat;
            }
          }
        } else {
          $this->cart->remove($row_id);
          $data2 = [
            "id" => $id,
            "qty" => $qty,
            "price" => $check->data->harga,
            "name" => $check->data->nama,
            "foto" => $check->data->foto,
            "stok" => $check->data->stok,
            "berat" => $check->data->berat
          ];
          $insert_cart = $this->cart->insert($data2);
          if ($insert_cart) {
            $berat = 0;
            $harga = $qty * $check->data->harga;
            $response->harga = "Rp. " . number_format($harga, "0", ",", ".") . " ,-";
            $response->total_harga = "Rp. " . number_format($this->cart->total(), "0", ",", ".") . " ,-";
            $response->success = TRUE;
            $response->berat = $qty * $check->data->berat;
            foreach ($this->cart->contents() as $item) {
              $berat += $item["qty"] * $item["berat"];
            }
            $response->total_berat = $berat;
          } else {
            $berat = 0;
            $response->success = TRUE;
            $response->harga = "Rp. 0 ,-";
            $response->harga_produk = $check->data->harga;
            $response->total_harga = "Rp. " . number_format($this->cart->total(), "0", ",", ".") . " ,-";
            $response->berat = 0;
            $response->total_berat = 0;
            foreach ($this->cart->contents() as $item) {
              $berat += $item["qty"] * $item["berat"];
            }
            $response->total_berat = $berat;
          }
        }
      } else {
        $harga = $check->data->harga * $qty1;
        $response->harga = "Rp. " . number_format($harga, "0", ",", ".") . " ,-";
        $response->success = FALSE;
        $response->message = "Maaf stok tidak mencukupi!";
      }
    } else {
      $response->message = "Maaf id barang tidak ditemukan!";
      $response->success = FALSE;
    }
    echo json_encode($response);
  }

  public function delete_cart()
  {
    if ($this->session->login !== TRUE) {
      $id = html_escape($this->input->get("id"));
      $this->cart->remove($id);
    } else {
      $cart_id = decrypt_url(html_escape($this->input->get("id")));
      $this->M_data->delete_data(["cart_id" => $cart_id], "list_cart");
    }
    // $this->session->set_flashdata('pesan', '<script>sweet("success", "Sukses", "Barang berhasil dihapus dari keranjang!")</script>');
    redirect("keranjang");
  }

  public function clear_cart()
  {
    if ($this->session->login === TRUE) {
      $this->M_data->delete_data(["user_id" => $this->session->user_id], "list_cart");
    } else {
      $this->cart->destroy();
    }
    $this->session->set_flashdata('pesan', '<script>sweet("success", "Sukses", "Barang berhasil dihapus dari keranjang!")</script>');
    redirect("keranjang");
  }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan extends CI_Controller {

  public function view_pesanan_masuk_management()
  {
    $start_date = html_escape($this->input->get("start_date"));
    $end_date = html_escape($this->input->get("end_date"));
    if ($start_date == NULL) {
      $start_date = date("Y-m-01");
    }
    if ($end_date == NULL) {
      $end_date = date("Y-m-t");
    }
    $data = [
      "user_info" => $this->M_data->user_info()->data,
      "list_menu" => $this->M_data->get_menu($this->session->userdata("role_id")),
      "title" => "Admin - Pesanan Masuk Management",
      "title_admin" => "Pesanan Masuk Management",
      "list_pesanan" => $this->M_data->get_pesanan_masuk($start_date, $end_date),
      "start_date" => $start_date,
      "end_date" => $end_date,
      "isi" => "admin/v_pesanan_masuk"
    ];
    $this->load->view('layout/v_wrapper', $data, FALSE);
  }
}
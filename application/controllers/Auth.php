<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->session->unset_userdata(["login", "user_id", "role"]);
  }

  public function index()
  {
    $this->form_validation->set_rules('username', 'Username', 'required', [
      "required" => "Wajib masukan username!"
    ]);
    $this->form_validation->set_rules('password', 'Password', 'required', [
      "required" => "Wajib masukan password!"
    ]);
    
    if ($this->form_validation->run() === TRUE) {
      $input = (object) html_escape($this->db->escape_str($this->input->post()));
      $this->user_login->login_admin($input);
    }
    $data = [
      "title" => "Login - SansShop"
    ];
    $this->load->view('v_login', $data, FALSE);
  }

  public function view_register_page()
  {
    $this->form_validation->set_rules('nama', 'Register', 'required',[
      "required" => "Wajib masukan nama lengkap!"
    ]);
    $this->form_validation->set_rules('username', 'Register','required', [
      "required" => "Wajib masukan username!"
    ]);
    $this->form_validation->set_rules('email', 'Register', 'required|valid_email|is_unique[list_user.email]',[
      "required" => "Wajib masukan email!",
      "valid_email" => "Masukan format email dengan benar!",
      "is_unique" => "Maaf, email sudah terdaftar"
    ]);
    $this->form_validation->set_rules('phone', 'Register', 'required|numeric', [
      "required" => "Wajib masukan no hp!",
      "numeric" => "Masukan no hp berupa angka!"
    ]);
    $this->form_validation->set_rules('password', 'Register', 'required|matches[re_password]', [
      "required" => "Wajib masukan password!",
      "matches" => "Password tidak sesuai!"
    ]);
    $this->form_validation->set_rules('re_password', 'Register','required|matches[password]', [
      "required" => "Wajib masukan password!",
      "matches" => "Password tidak sesuai!"
    ]);

    if ($this->form_validation->run() == FALSE) {
      $this->load->view("v_register");
    } else {
      $this->process_register_user();
    }
  }

  private function process_register_user()
  {
    $input = (object)html_escape($this->input->post());
    $data = [
      "nama" => $this->db->escape_str($input->nama),
      "email" => $this->db->escape_str($input->email),
      "phone" => $this->db->escape_str($input->phone),
      "username" => $this->db->escape_str($input->username),
      "password" => password_hash($input->password, PASSWORD_DEFAULT),
      "role_id" => 3
    ];
    $check = $this->M_data->insert_data($data, "list_user");
    if ($check->success === TRUE) {
      $this->session->set_flashdata('pesan', '<script>sweet("success", "Sukses!", "Sukses mendaftarkan akun anda! Silahkan login!")</script>');
      redirect("login");
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal!", "Silahkan register ulang!")</script>');
      redirect("register");
    }
  }

  public function logout()
  {
    $this->user_login->logout_admin();
  }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_admin extends CI_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->user_login->protect_login();
    if ($this->session->userdata("role_id") != 1) {
      redirect("login");
    }
  }

  public function view_dashboard_management()
  {
    $data = [
      "user_info" => $this->M_data->user_info()->data,
      "list_menu" => $this->M_data->get_menu($this->session->userdata("role_id")),
      "title" => "Admin - Dashboard",
      "title_admin" => "Dashboard",
      "isi" => "admin/v_dashboard",
      "total_barang" => $this->M_data->get_table_rows("list_barang"),
      "total_kategori" => $this->M_data->get_table_rows("list_kategori_barang"),
      "total_user" => $this->M_data->get_table_rows("list_user")
    ];
    $this->load->view('layout/v_wrapper', $data, FALSE);
  }

  public function view_menu_management()
  {
    $data = [
      "user_info" => $this->M_data->user_info()->data,
      "list_menu" => $this->M_data->get_menu($this->session->userdata("role_id")),
      "menu" => $this->M_data->get_data("list_menu")->data,
      "list_access" => $this->M_data->get_access(),
      "list_role" => $this->M_data->get_data("list_role_user")->data,
      "title" => "Admin - Menu",
      "title_admin" => "Menu Management",
      "isi" => "admin/v_menu"
    ];
    $this->load->view('layout/v_wrapper', $data, FALSE);
  }

  public function process_add_menu()
  {
    $input = (object)html_escape($this->db->escape_str($this->input->post()));
    $data = [
      "menu" => $input->menu
    ];
    $check = $this->M_data->insert_data($data, "list_menu");
    if ($check->success === TRUE) {
      $this->session->set_flashdata("pesan", "<div class='alert alert-success alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Sukses!</strong> Menu berhasil ditambah!.
      </div>");
      redirect("menu");
    } else {
      $this->session->set_flashdata("pesan", "<div class='alert alert-primary alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Gagal!</strong> $check->debug.
      </div>");
      redirect("menu");
    }
  }

  public function view_menu_edit()
  {
    $response = new stdClass();
    $id = html_escape($this->input->get("id"));
    $where = ["menu_id" => $id];
    $check = $this->M_data->edit_data($where, "list_menu");
    if ($check->success === TRUE) {
      $response->success = 200;
      $response->data = $check->data;
    } else {
      $response->success = 201;
    }
    echo json_encode($response);
  }

  public function process_menu_edit()
  {
    $input = (object)html_escape($this->db->escape_str($this->input->post()));
    $data = ["menu" => $input->menu_edit];
    $where = ["menu_id" => $input->menu_id];
    $check = $this->M_data->update_data($data, $where, "list_menu");
    if ($check->success === TRUE) {
      $this->session->set_flashdata("pesan", "<div class='alert alert-success alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Sukses!</strong> Menu berhasil diupdate!.
      </div>");
      redirect("menu");
    } else {
      $this->session->set_flashdata("pesan", "<div class='alert alert-primary alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Gagal!</strong> Menu gagal diupdate!.
      </div>");
      redirect("menu");
    }
  }

  public function process_menu_delete($id)
  {
    $menu_id = (int)html_escape($this->db->escape_str($id));
    $where = ["menu_id" => $menu_id];
    $check = $this->M_data->delete_data($where, "list_menu");
    if ($check->success) {
      $this->session->set_flashdata("pesan", "<div class='alert alert-success alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Sukses!</strong> Menu berhasil dihapus!.
      </div>");
      redirect("menu");
    } else {
      $this->session->set_flashdata("pesan", "<div class='alert alert-primary alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Gagal!</strong> $check->debug.
      </div>");
      redirect("menu");
    }
  }

  public function process_access_add()
  {
    $input = (object)html_escape($this->db->escape_str($this->input->post()));
    $data = [
      "menu_id" => $input->menu_id,
      "role_id" => $input->role_id
    ];
    $check = $this->M_data->insert_data($data, "rel_access");
    if ($check->success === TRUE) {
      $this->session->set_flashdata("pesan2", "<div class='alert alert-success alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Sukses!</strong> Hak akses berhasil ditambah!.
      </div>");
      redirect("menu");
    } else {
      $this->session->set_flashdata("pesan2", "<div class='alert alert-danger alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Gagal!</strong> Hak akses gagal ditambah!.
      </div>");
      redirect("menu");
    }
  }

  public function view_access_edit()
  {
    $response = new stdClass();
    $access_id = (int)html_escape($this->db->escape_str($this->input->get("id")));
    $check = $this->M_data->get_access_detail($access_id);
    if ($check->success === TRUE) {
      $response->success = TRUE;
      $response->data = $check->data;
    } else {
      $response->success = FALSE;
    }
    echo json_encode($response);
  }

  public function process_access_edit()
  {
    $input = (object)html_escape($this->db->escape_str($this->input->post()));
    $data = [
      "menu_id" => $input->menu_id,
      "role_id" => $input->role_id
    ];
    $where = ["access_id" => $input->access_id];
    $check = $this->M_data->update_data($data, $where, "rel_access");
    if ($check->success === TRUE) {
      $this->session->set_flashdata("pesan2", "<div class='alert alert-success alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Sukses!</strong> Hak akses berhasil diupdate!.
      </div>");
      redirect("menu");
    } else {
      $this->session->set_flashdata("pesan2", "<div class='alert alert-danger alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Gagal!</strong> Hak akses gagal diupdate!.
      </div>");
      redirect("menu");
    }
  }

  public function process_access_delete($id)
  {
    $access_id = (int)html_escape($this->db->escape_str($id));
    $where = ["access_id" => $access_id];
    $check = $this->M_data->delete_data($where, "rel_access");
    if ($check->success === TRUE) {
      $this->session->set_flashdata("pesan2", "<div class='alert alert-success alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Sukses!</strong> Hak akses berhasil dihapus!.
      </div>");
      redirect("menu");
    } else {
      $this->session->set_flashdata("pesan2", "<div class='alert alert-danger alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Gagal!</strong> Hak akses gagal dihapus!.
      </div>");
      redirect("menu");
    }
  }

  public function view_submenu_management()
  {
    $data = [
      "user_info" => $this->M_data->user_info()->data,
      "list_menu" => $this->M_data->get_menu($this->session->userdata("role_id")),
      "title" => "Admin - Sub Menu",
      "title_admin" => "Sub Menu Management",
      "isi" => "admin/v_submenu",
      "menu" => $this->M_data->get_data("list_menu")->data,
      "list_sub" => $this->M_data->get_submenu()
    ];
    $this->load->view('layout/v_wrapper', $data, FALSE);
  }

  public function process_submenu_add()
  {
    $this->form_validation->set_rules('menu_id', 'Submenu', 'required');
    $this->form_validation->set_rules('title', 'Submenu', 'required');
    $this->form_validation->set_rules('icon', 'Submenu', 'required');
    $this->form_validation->set_rules('link', 'Submenu', 'required');
    $this->form_validation->set_rules('header', 'Submenu', 'required');
    
    if ($this->form_validation->run() == TRUE) {
      $input = (object)html_escape($this->db->escape_str($this->input->post()));
      $data = [
        "menu_id" => $input->menu_id,
        "title" => $input->title,
        "icon" => $input->icon,
        "link" => $input->link,
        "header" => $input->header
      ];
      $check = $this->M_data->insert_data($data, "list_sub_menu");
      if ($check->success === TRUE) {
        $this->session->set_flashdata("pesan", "<div class='alert alert-success alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Sukses!</strong> Submenu berhasil ditambah!.
      </div>");
        redirect("sub_menu");
      } else {
        $this->session->set_flashdata("pesan", "<div class='alert alert-danger alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Gagal!</strong> Submenu gagal ditambah!.
      </div>");
        redirect("sub_menu");
      }
    } else {
      $this->session->set_flashdata("pesan", "<div class='alert alert-danger alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Gagal!</strong> Data input tidak valid!.
      </div>");
      redirect("sub_menu");
    }
  }

  public function view_submenu_edit()
  {
    $response = new stdClass();
    $sub_id = (int)html_escape($this->input->get("id"));
    $check = $this->M_data->edit_data(["sub_id" => $sub_id], "list_sub_menu");
    if ($check->success === TRUE) {
      $response->data = $check->data;
    } else {
      $response->success = FALSE;
    }
    echo json_encode($response);
  }

  public function process_submenu_edit()
  {
    $this->form_validation->set_rules('sub_id', 'Submenu', 'required');
    $this->form_validation->set_rules('menu_id', 'Submenu', 'required');
    $this->form_validation->set_rules('title', 'Submenu', 'required');
    $this->form_validation->set_rules('icon', 'Submenu', 'required');
    $this->form_validation->set_rules('link', 'Submenu', 'required');
    $this->form_validation->set_rules('header', 'Submenu', 'required');

    if ($this->form_validation->run() == TRUE) {
      $input = (object)html_escape($this->db->escape_str($this->input->post()));
      $data = [
        "menu_id" => $input->menu_id,
        "title" => $input->title,
        "icon" => $input->icon,
        "link" => $input->link,
        "header" => $input->header
      ];
      $where = ["sub_id" => $input->sub_id];
      $check = $this->M_data->update_data($data, $where, "list_sub_menu");
      if ($check->success === TRUE) {
        $this->session->set_flashdata("pesan", "<div class='alert alert-success alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Sukses!</strong> Submenu berhasil diupdate!.
      </div>");
        redirect("sub_menu");
      } else {
        $this->session->set_flashdata("pesan", "<div class='alert alert-danger alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Gagal!</strong> Submenu gagal diupdate!.
      </div>");
        redirect("sub_menu");
      }
    } else {
      $this->session->set_flashdata("pesan", "<div class='alert alert-danger alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Gagal!</strong> Data input tidak valid!.
      </div>");
      redirect("sub_menu");
    }
  }

  public function process_submenu_delete($id)
  {
    $sub_id = (int)html_escape($this->db->escape_str($id));
    $where = ["sub_id" => $sub_id];
    $check = $this->M_data->delete_data($where, "list_sub_menu");
    if ($check->success === TRUE) {
      $this->session->set_flashdata("pesan", "<div class='alert alert-success alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Sukses!</strong> Submenu berhasil dihapus!.
      </div>");
      redirect("sub_menu");
    } else {
      $this->session->set_flashdata("pesan", "<div class='alert alert-danger alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Gagal!</strong> Submenu gagal dihapus!.
      </div>");
      redirect("sub_menu");
    }
  }

  public function view_admin_management()
  {
    $data = [
      "user_info" => $this->M_data->user_info()->data,
      "list_menu" => $this->M_data->get_menu($this->session->userdata("role_id")),
      "title" => "Admin - Admin Management",
      "title_admin" => "Admin Management",
      "isi" => "admin/v_admin_management",
      "list_admin" => $this->M_data->get_list_admin()->data->result()
    ];
    $this->load->view('layout/v_wrapper', $data, FALSE);
  }

  public function validate_admin_add()
  {
    $this->form_validation->set_rules('nama', 'Admin', 'required');
    $this->form_validation->set_rules('username', 'Admin', 'required');
    $this->form_validation->set_rules('email', 'Admin', 'required|valid_email');
    $this->form_validation->set_rules('password', 'Admin', 'required');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Isi data dengan benar & lengkap!")</script>');
      redirect("admin");
    } else {
      $this->process_admin_add();
    } 
  }

  private function process_admin_add()
  {
    $input = (object)html_escape($this->db->escape_str($this->input->post()));
    $data = [
      "nama" => $input->nama,
      "username" => $input->username,
      "email" => $input->email,
      "password" => password_hash($input->password, PASSWORD_DEFAULT),
      "role_id" => 2
    ];
    $check = $this->M_data->insert_data($data, "list_user");
    if($check->success === TRUE) {
      $this->session->set_flashdata('pesan', '<script>sweet("success", "Sukses", "Sukses menambahkan admin!")</script>');
      redirect("admin");
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Gagal menambahkan admin!")</script>');
    }
  }

  public function view_admin_detail()
  {
    $response = new stdClass();
    $admin_id = (int)html_escape($this->db->escape_str($this->input->get("id")));
    $where = ["user_id" => $admin_id];
    $check = $this->M_data->edit_data($where, "list_user");
    if ($check->success === TRUE) {
      $response->success = 200;
      $data = [
        "user_id" => $check->data->user_id,
        "nama" => $check->data->nama, 
        "username" => $check->data->username,
        "email" => $check->data->email 
      ];
      $response->data = $data;
    } else {
      $response->success = 201;
    }
    echo json_encode($response);
  }

  public function validate_admin_edit()
  {
    $this->form_validation->set_rules('nama', 'Admin', 'required');
    $this->form_validation->set_rules('username', 'Admin', 'required');
    $this->form_validation->set_rules('email', 'Admin', 'required|valid_email');

    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Isi data dengan benar & lengkap!")</script>');
      redirect("admin");
    } else {
      $this->process_admin_edit();
    }
  }

  private function process_admin_edit()
  {
    $input = (object)html_escape($this->db->escape_str($this->input->post()));
    $data = [
      "nama" => $input->nama,
      "username" => $input->username,
      "email" => $input->email
    ];
    if (isset($input->password) && $input->password != ""){
      $data["password"] = password_hash($input->password, PASSWORD_DEFAULT); 
    }
    $check = $this->M_data->update_data($data, ["user_id" => $input->user_id], "list_user");
    if ($check->success === TRUE) {
      $this->session->set_flashdata('pesan', '<script>sweet("success", "Suksesl", "Sukses update admin!")</script>');
      redirect("admin");
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Gagal update admin!")</script>');
      redirect("admin");
    }
  }

  public function process_admin_delete($user_id)
  {
    $admin_id = (int)html_escape($user_id);
    $check = $this->M_data->delete_data(["user_id" => $admin_id], "list_user");
    if ($check->success === TRUE) {
      $this->session->set_flashdata('pesan', '<script>sweet("success", "Suksesl", "Sukses menghapus admin!")</script>');
      redirect("admin");
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Gagal menghapus admin!")</script>');
      redirect("admin");
    }
  }
}
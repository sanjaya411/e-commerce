<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Checkout extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function view_checkout_page()
  {
    $login = $this->session->login;
    if ($login !== TRUE) {
      $cart = $this->cart->total();
      if ($cart == 0) {
        $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal masuk!", "Maaf anda wajib memiliki minimal satu barang belanja!")</script>');
        redirect("home");
      } else {
        $data = [
          "user" => $this->M_data->get_user_detail()->data,
          "title" => "Checkout",
          "isi" => "user/v_checkout",
          "cart" => $this->cart->contents(),
          "kategori" => $this->M_data->edit_data(["active" => 1], "list_kategori_barang")->data
        ];
        $this->load->view("layout/v_wrapper_fe", $data);
      }
    } else {
      $cart = $this->M_data->get_cart_user();
      if ($cart->total > 0) {
        $data = [
          "user" => $this->M_data->get_user_detail()->data,
          "title" => "Checkout",
          "isi" => "user/v_checkout",
          "cart" => $cart->data,
          "kategori" => $this->M_data->edit_data(["active" => 1], "list_kategori_barang")->data
        ];
        $this->load->view("layout/v_wrapper_fe", $data);
      } else {
        $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal masuk!", "Maaf anda wajib memiliki minimal satu barang belanja!")</script>');
        redirect("home");
      }
    }
  }

  public function process_invoice_create()
  {
    $this->form_validation->set_rules('nama_penerima', 'Checkout', 'required');
    $this->form_validation->set_rules('no_hp_penerima', 'Checkout', 'required|numeric');
    $this->form_validation->set_rules('email_penerima', 'Checkout', 'required|valid_email');
    $this->form_validation->set_rules('provinsi_id', 'Checkout', 'required');
    $this->form_validation->set_rules('kota_id', 'Checkout', 'required');
    $this->form_validation->set_rules('expedisi_id', 'Checkout', 'required');
    $this->form_validation->set_rules('expedisi', 'Checkout', 'required');
    $this->form_validation->set_rules('alamat_tujuan', 'Checkout', 'required');
    $this->form_validation->set_rules('harga_ongkir', 'Checkout', 'required');
    $this->form_validation->set_rules('total_bayar', 'Checkout', 'required');

    if ($this->form_validation->run() == TRUE) {
      $input = (object)html_escape($this->input->post());
      $check = $this->M_data->create_invoice($input);
      // var_dump($check);
      if ($check->success === TRUE) {
        $this->session->set_flashdata("pesan", "<script>sweet('success', 'Sukses!', '$check->message')</script>");  
        $url = encrypt_url($check->id);
        redirect("invoice?id=$check->id&token=$url");
      } else {
        // var_dump($check);
        $this->session->set_flashdata("pesan", "<script>sweet('error', 'Gagal!', '" . $check->message . "')</script>");
        // $this->session->set_flashdata("pesan", $check->message);
        redirect("checkout");
      }
    } else {
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Gagal!', 'Isi data dengan benar & lengkap!')</script>");
      redirect("checkout");
    }
  }

  public function view_invoice_page()
  {
    $url = decrypt_url($this->input->get("token"));
    $id = (int)html_escape($this->input->get("id", TRUE));
    if ($url != "" && $id != "" && $url == $id) {
      $check = $this->M_data->get_invoice_detail($id);
      if ($check->success) {
        $data = [
          "user" => $this->M_data->get_user_detail()->data,
          "title" => "Invoice",
          "isi" => "user/v_invoice",
          "kategori" => $this->M_data->edit_data(["active" => 1], "list_kategori_barang")->data,
          "list_barang" => $check->barang,
          "invoice" => $check->invoice,
          "pengiriman" => $check->pengiriman,
          "pengiriman_dari" => $check->pengiriman_dari
        ];
        $this->load->view("layout/v_wrapper_fe", $data, FALSE);
      } else {
        $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "' . $check->message . '")</script>');
        redirect("home");
      }
    } else {
      redirect("home");
    }
  }

  public function view_pesanan_saya()
  {
    $this->user_login->protect_login();
    $check = $this->M_data->get_invoice_listed();
    if ($check->success === TRUE) {
      $data = [
        "user" => $this->M_data->get_user_detail()->data,
        "title" => "Pesanan Saya",
        "isi" => "user/v_my_invoice",
        "kategori" => $this->M_data->edit_data(["active" => 1], "list_kategori_barang")->data,
        "list_invoice" => $check->data
      ];
      $this->load->view("layout/v_wrapper_fe", $data, FALSE);
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "' . $check->message . '")</script>');
      redirect("home");
    }
  }
}

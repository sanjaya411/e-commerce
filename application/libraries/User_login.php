<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_login
{
  protected $ci;

  public function __construct()
  {
    $this->ci =& get_instance();
    $this->ci->load->model("M_data");
  }

  public function login_admin($input)
  {
    $check = $this->ci->M_data->edit_data(["username" => $input->username], "list_user");
    if ($check->total > 0) {
      if (password_verify($input->password, $check->data->password)) {
        $array = array(
          "user_id" => $check->data->user_id,
          "role_id" => $check->data->role_id,
          "login" => TRUE
        );
        $this->ci->session->set_userdata($array);
        if ($check->data->role_id == 1) {
          redirect("dashboard");
        } else if ($check->data->role_id == 2) {
          redirect("user");
        } else {
          redirect("index");
        }
      } else {
        $this->ci->session->set_flashdata("pesan", "<div class='alert alert-danger alert-dismissible fade show'>
          <button type='button' class='close' data-dismiss='alert'>&times;</button>
          <strong>Failed!</strong> Password is Wrong!.
        </div>");
        redirect("login");
      }
    } else {
      $this->ci->session->set_flashdata("pesan", "<div class='alert alert-danger alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Failed!</strong> Username is wrong!.
      </div>");
      redirect("login");
    }
  }

  public function protect_login()
  {
    if ($this->ci->session->userdata("login") !== TRUE) {
      $this->ci->session->set_flashdata("pesan", "<div class='alert alert-danger alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <strong>Failed!</strong> You must login!.
      </div>");
      redirect("login");
    }
  }

  public function logout_admin()
  {
    $this->ci->session->unset_userdata(["user_id", "login", "role"]);
    $this->ci->session->set_flashdata("pesan", "<div class='alert alert-info alert-dismissible fade show'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        Success logout!.
      </div>");
    redirect("login");
  }
}
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Invoice
{
  protected $ci;

  public function __construct()
  {
    $this->ci = &get_instance();
    $this->ci->load->model("M_data");
  }

  public function check_invoice()
  {
    $date = new DateTime("now", new DateTimeZone("Asia/Jakarta"));
    $now = $date->format("Y-m-d H:i:s");
    $sql = "SELECT * FROM `list_invoice` "
      .  "WHERE `tanggal_jatuh_tempo` <= '$now' AND `invoice_status_id` != 3;";
    $query = $this->ci->db->query($sql);
    if ($query) {
      foreach ($query->result() as $item) {
        $sql2 = "UPDATE `list_invoice` SET "
          . "`invoice_status_id` = 3 "
          . "WHERE `invoice_id` = ?;";
        $query2 = $this->ci->db->query($sql2, [$item->invoice_id]);
        if (!$query2) {
          echo "Query 2 failed!";
        } else {
          $sql3 = "SELECT * FROM `list_checkout` "
            . "WHERE `invoice_id` = ?;";
          $query3 = $this->ci->db->query($sql3, [$item->invoice_id]);
          if ($query3) {
            foreach ($query3->result() as $items) {
              $sql4 = "SELECT * FROM `list_barang` "
                . "WHERE `barang_id` = ?;";
              $query4 = $this->ci->db->query($sql4, [$items->barang_id]);
              if ($query4) {
                $barang = $query4->row();
                $sisa_barang = $barang->stok + $items->jumlah_barang;
                $sql5 = "UPDATE `list_barang` SET "
                  . "`stok` = ? "
                  . "WHERE `barang_id` = ?;";
                $query5 = $this->ci->db->query($sql5, [$sisa_barang, $barang->barang_id]);
                if (!$query5) {
                  echo "Query 5 failed!";
                }
              } else {
                echo "Query 4 failed!";
              }
            }
          } else {
            echo "Query 3 failed";
          }
        }
      }
    } else {
      echo "Query 1 failed!";
    }
  }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_data extends CI_Model
{
  public function get_data($table)
  {
    $response = new stdClass();
    $query = $this->db->get($table);
    if ($query) {
      $response->success = TRUE;
      $response->data = $query;
      $response->query = $query;
    } else {
      $response->success = FALSE;
    }
    return $response;
  }

  public function insert_data($data, $table)
  {
    $response = new stdClass();
    $query = $this->db->insert($table, $data);
    if ($query) {
      $response->success = TRUE;
    } else {
      $response->success = FALSE;
      $response->debug = $this->db->error();
    }
    return $response;
  }

  public function edit_data($where, $table)
  {
    $response = new stdClass();
    $query = $this->db->get_where($table, $where);
    if ($query) {
      $response->success = TRUE;
      $response->total = $query->num_rows();
      if ($query->num_rows() > 1) {
        $response->data = $query->result();
      } else {
        $response->data = $query->row();
      }
      $response->query = $query;
    } else {
      $response->success = FALSE;
    }
    $response->debug = $this->db->last_query();
    return $response;
  }

  public function update_data($data, $where, $table)
  {
    $response = new stdClass();
    $query = $this->db->set($data)->where($where)->update($table);
    if ($query) {
      $response->success = TRUE;
    } else {
      $response->success = FALSE;
    }
    return $response;
  }

  public function delete_data($where, $table)
  {
    $response = new stdClass();
    $query = $this->db->delete($table, $where);
    if ($query) {
      $response->success = TRUE;
    } else {
      $response->success = FALSE;
    }
    return $response;
  }

  public function get_user()
  {
    $response = new stdClass();
    $query = $this->db->select("*")
      ->from("`list_user` ls")
      ->join("`list_role_user` lrs", "ls.`role_id` = lrs.`role_id`")
      ->where("ls.`role_id`", 3)
      ->get();
    if ($query) {
      $response->success = TRUE;
      $response->data = $query;
    } else {
      $response->success = FALSE;
    }
    return $response;
  }

  public function get_list_admin()
  {
    $response = new stdClass();
    $query = $this->db->select("*")
      ->from("`list_user` ls")
      ->join("`list_role_user` lrs", "ls.`role_id` = lrs.`role_id`")
      ->where("ls.`role_id`", 2)
      ->get();
    if ($query) {
      $response->success = TRUE;
      $response->data = $query;
    } else {
      $response->success = FALSE;
    }
    return $response;
  }

  public function login_user($input)
  {
    $response = new stdClass();
    $query = $this->db->select("*")
      ->from("list_user")
      ->where(
        [
          "username" => $input->username,
          "password" => $input->password
        ]
      )->get();
    if ($query->num_rows() > 0) {
      $response->success = TRUE;
      $response->data = $query->row();
    } else {
      $response->success = FALSE;
    }
    return $response;
  }

  public function user_info()
  {
    $admin_id = $this->session->userdata("user_id");
    $response = new stdClass();
    $query = $this->db->select("*")
      ->from("`list_user` ls")
      ->join("`list_role_user` lrs", "ls.`role_id` = lrs.`role_id`")
      ->where("ls.`user_id`", $admin_id)
      ->get();
    if ($query->num_rows() > 0) {
      $response->success = TRUE;
      $response->data = $query->row();
    } else {
      $response->success = FALSE;
    }
    return $response;
  }

  public function get_menu($role_id)
  {
    $query = $this->db->select("*")
      ->from("`list_menu` lm")
      ->join("`rel_access` ra", "ra.`menu_id` = lm.`menu_id`")
      ->where("ra.`role_id`", $role_id)
      ->get();
    if ($query) {
      return $query->result();
    } else {
      return FALSE;
    }
  }

  public function get_access()
  {
    $query = $this->db->select("*")
      ->from("`rel_access` ra")
      ->join("`list_menu` lm", "ra.`menu_id` = lm.`menu_id`")
      ->join("`list_role_user` lrs", "lrs.`role_id` = ra.`role_id`")
      ->get();
    if ($query) {
      return $query->result();
    } else {
      return FALSE;
    }
  }

  public function get_access_detail($id)
  {
    $response = new stdClass();
    $query = $this->db->select("*")
      ->from("`rel_access` ra")
      ->join("`list_menu` lm", "ra.`menu_id` = lm.`menu_id`")
      ->join("`list_role_user` lrs", "lrs.`role_id` = ra.`role_id`")
      ->where("ra.`access_id`", $id)
      ->get();
    if ($query) {
      $response->success = TRUE;
      $response->data = $query->row();
    } else {
      $response->success = FALSE;
    }
    return $response;
  }

  public function get_submenu()
  {
    $query = $this->db->select("*")
      ->from("`list_sub_menu` lsm")
      ->join("`list_menu` lm", "lm.`menu_id` = lsm.`menu_id`")
      ->get();
    if ($query) {
      return $query->result();
    } else {
      return FALSE;
    }
  }

  public function get_user_detail($user_id = FALSE)
  {
    if ($user_id === FALSE) {
      $user_id = $this->session->user_id;
    }
    $response = new stdClass();
    $query = $this->db->select("`user_id`, `nama`, `email`, `username`, `role_id`")
      ->from("list_user")
      ->where("user_id", $user_id)
      ->get();
    if ($query) {
      $response->success = TRUE;
      $response->data = $query->row();
    } else {
      $response->success = FALSE;
    }
    return $response;
  }

  public function get_barang_list()
  {
    $response = new stdClass();
    $query = $this->db->select("lb.*, lkb.`nama` AS `nama_kategori`")
      ->from("`list_barang` lb")
      ->join("`list_kategori_barang` lkb", "lb.`kategori_id` = lkb.`kategori_id`")
      ->order_by("lb.`barang_id` ASC")
      ->get();
    if ($query) {
      $response->success = TRUE;
      $response->total = $query->num_rows();
      $response->data = $query->result();
    } else {
      $response->success = FALSE;
    }
    return $response;
  }

  public function get_barang_list_home()
  {
    $response = new stdClass();
    $query = $this->db->select("lb.*, lkb.`nama` AS `nama_kategori`")
      ->from("`list_barang` lb")
      ->join("`list_kategori_barang` lkb", "lb.`kategori_id` = lkb.`kategori_id`")
      ->where("stok > 0")
      ->limit(8)
      ->get();
    if ($query) {
      $query2 = $this->db->get("list_barang");
      if ($query2) {
        $response->total = $query2->num_rows();
      }
      $response->success = TRUE;
      $response->data = $query->result();
    } else {
      $response->success = FALSE;
    }
    return $response;
  }

  public function get_table_rows($table)
  {
    return $this->db->count_all($table);
  }

  public function get_barang_detail($barang_id)
  {
    $response = new stdClass();
    $query = $this->db->select("lb.*, lkb.`nama` AS `nama_kategori`, lkb.`kategori_id` AS `kategori`")
      ->from("`list_barang` lb")
      ->join("`list_kategori_barang` lkb", "lb.`kategori_id` = lkb.`kategori_id`")
      ->where("lb.`barang_id`", $barang_id)
      ->get();
    if ($query) {
      $response->success = TRUE;
      $response->data = $query->row();
    } else {
      $response->success = FALSE;
    }
    return $response;
  }

  public function get_barang_kategori($page, $id)
  {
    $start = ($page - 1) * 12;
    $sql = "SELECT lb.*, lkb.`nama` AS `nama_kategori` FROM `list_barang` lb "
      .  "JOIN `list_kategori_barang` lkb ON lkb.`kategori_id` = lb.`kategori_id` "
      .  "WHERE lb.`kategori_id` = ? AND lb.`stok` > 0 "
      .  "LIMIT 12 OFFSET $start;";
    $query = $this->db->query($sql, [$id]);
    if ($query) {
      return $query->result();
      // return $this->db->last_query();
    } else {
      return FALSE;
      // return $this->db->last_query();
      // return var_dump($page);
    }
  }

  public function get_count_barang_kategori($id)
  {
    $query = $this->db->select("COUNT(`barang_id`) AS `total`")
      ->from("`list_barang` lb")
      ->join("`list_kategori_barang` lkb", "lkb.`kategori_id` = lb.`kategori_id`")
      ->where("lb.`kategori_id`", $id)
      ->where("lb.`stok` > 0")
      ->get();
    if ($query) {
      return $query->row()->total;
    } else {
      return FALSE;
    }
  }

  public function get_pagination_barang_kategori($id)
  {
    $item_per_page = 12;
    $count = $this->get_count_barang_kategori($id);
    $total = ceil($count / $item_per_page);
    return $total;
  }

  public function get_foto_barang($id)
  {
    $query = $this->db->select("*")
      ->from("list_gambar_barang")
      ->where("barang_id", $id)
      ->get();
    if ($query) {
      return $query->result();
    } else {
      return FALSE;
    }
  }

  public function get_count_barang()
  {
    $query = $this->db->select("COUNT(`barang_id`) AS `total`")
      ->from("`list_barang` lb")
      ->join("`list_kategori_barang` lkb", "lkb.`kategori_id` = lb.`kategori_id`")
      ->where("lb.`stok` > 0")
      ->get();
    if ($query) {
      return $query->row()->total;
    } else {
      return FALSE;
    }
  }

  public function get_pagination_barang()
  {
    $item_per_page = 12;
    $count = $this->get_count_barang();
    $total = ceil($count / $item_per_page);
    return $total;
  }

  public function get_all_barang($page)
  {
    $response = new stdClass();
    $start = ($page - 1) * 12;
    $sql = "SELECT lb.*, lkb.`nama` AS `nama_kategori` FROM `list_barang` lb "
      .  "JOIN `list_kategori_barang` lkb ON lkb.`kategori_id` = lb.`kategori_id` "
      .  "WHERE lb.`stok` > 0 "
      .  "LIMIT 12 OFFSET $start";
    $query = $this->db->query($sql);
    if ($query) {
      $response->success = TRUE;
      $response->total = $this->get_data("list_barang")->query->num_rows();
      $response->data = $query->result();
    } else {
      $response->success = FALSE;
    }
    return $response;
  }

  public function create_invoice($input)
  {
    date_default_timezone_set("Asia/Jakarta");
    $response = new stdClass();
    $error = FALSE;
    $date = date("Ymd");
    $this->db->trans_begin();
    $accept_invoice = FALSE;
    $invoice_number = "";
    do {
      $sql = "SELECT `invoice_number` FROM `list_invoice` "
        .  "WHERE 1 "
        .  "GROUP BY `invoice_number` DESC "
        .  "LIMIT 1 "
        .  "FOR UPDATE;";
      $query = $this->db->query($sql);
      if ($query) {
        if ($query->num_rows() > 0) {
          $array = explode("-", $query->row()->invoice_number);
          $number = (int)$array[2];
          $number++;
          $number = "0000$number";
          $invoice_number = "INV-$date-" . substr($number, -4);
          $accept_invoice = $this->check_duplicate("list_invoce", "invoice_number", $invoice_number);
          if ($accept_invoice === TRUE) {
            $error = TRUE;
            $response->message = "Duplicate invoice number!";
          }
        } else {
          $invoice_number = "INV-$date-0001";
          $accept_invoice = TRUE;
        }
      } else {
        $error = TRUE;
        $response->message = "Query 1 failed!";
        $accept_invoice = TRUE;
      }
    } while ($accept_invoice === FALSE);
    $user = NULL;
    if ($this->session->userdata("login") === TRUE) {
      $user = $this->session->userdata("user_id");
    }
    $tanggal_jatuh_tempo = date("Y-m-d H:i", strtotime("+2hours"));
    $sql2 = "INSERT INTO `list_invoice` "
      . "(`invoice_number`, `user_id`, `nama_penerima`, `no_hp_penerima`, `email_penerima`, `created_date`, `tanggal_jatuh_tempo`, `total_bayar`, `invoice_status_id`) "
      . "VALUES (?, ?, ?, ?, ?, NOW(), ?, ?, 1);";
    $query2 = $this->db->query($sql2, [$invoice_number, $user, $input->nama_penerima, $input->no_hp_penerima, $input->email_penerima, $tanggal_jatuh_tempo, $input->total_bayar]);
    if ($query2) {
      // $response->message = $this->db->last_query();
      $response->id = $invoice_id = $this->db->insert_id();
      if ($this->session->userdata("login") === TRUE) {
        $sql3 = "SELECT * FROM `list_cart` "
          . "WHERE `user_id` = ?;";
        $query3 = $this->db->query($sql3, [$this->session->user_id]);
        if ($query3) {
          $values = "";
          $delimiter = "";
          foreach ($query3->result() as $item) {
            $sql4 = "SELECT * FROM `list_barang` "
              . "WHERE `barang_id` = ?;";
            $query4 = $this->db->query($sql4, [$item->barang_id]);
            if ($query4) {
              $barang = $query4->row();
              $sisa_barang = $barang->stok - $item->jumlah_barang;
              $sql5 = "UPDATE `list_barang` SET "
                . "`stok` = ? "
                . "WHERE `barang_id` = ?;";
              $query5 = $this->db->query($sql5, [$sisa_barang, $barang->barang_id]);
              if (!$query5) {
                $error = TRUE;
                $response->message = "Query 5 failed!";
              }
            } else {
              $error = TRUE;
              $response->message = "Query 4 failed!";
            }

            $values .= "$delimiter ($invoice_id, $item->barang_id, $item->jumlah_barang)";
            $delimiter = ",";
          }
          $sql6 = "INSERT INTO `list_checkout` "
            . "(`invoice_id`, `barang_id`, `jumlah_barang`) "
            . "VALUES $values;";
          $query6 = $this->db->query($sql6);
          if ($query6) {
            $strpos = strpos($input->expedisi, "Rp");
            $expedisi = substr($input->expedisi, 0, $strpos);
            $sql7 = "INSERT INTO `list_pengiriman` "
              . "(`invoice_id`, `expedisi`, `alamat_tujuan`, `harga_ongkir`) "
              . "VALUES (?, ?, ?, ?);";
            $query7 = $this->db->query($sql7, [$invoice_id, $expedisi, $input->alamat_tujuan, $input->harga_ongkir]);
            if ($query7) {
              $sql8 = "DELETE FROM `list_cart` "
                . "WHERE `user_id` = ?;";
              $query8 = $this->db->query($sql8, [$this->session->user_id]);
              if (!$query8) {
                $error = TRUE;
                $response->message = "Query 8 failed!";
              }
            } else {
              $error = TRUE;
              $response->message = "Query 7 failed!";
            }
          } else {
            $error = TRUE;
            $response->message = "Query 6 failed!";
            // var_dump($this->db->last_query());
          }
        } else {
          $error = TRUE;
          $response->message = "Query 3 failed!";
        }
      } else {
        $values = "";
        $delimiter = "";
        foreach ($this->cart->contents() as $item) {
          $sql3 = "SELECT * FROM `list_barang` "
            . "WHERE `barang_id` = ?;";
          $query3 = $this->db->query($sql3, [$item["id"]]);
          if ($query3) {
            $barang = $query3->row();
            $sisa_barang = $barang->stok - $item["qty"];
            $sql4 = "UPDATE `list_barang` SET "
              . "`stok` = ? "
              . "WHERE `barang_id` = ?;";
            $query4 = $this->db->query($sql4, [$sisa_barang, $barang->barang_id]);
            if (!$query4) {
              $error = TRUE;
              $response->message = "Query 4 failed!";
            }
          } else {
            $error = TRUE;
            $response->message = "Query 3 failed!";
          }

          $values .= "$delimiter ($invoice_id, " . $item["id"] . ", " . $item["qty"] . ")";
          $delimiter = ",";
        }
        $sql5 = "INSERT INTO `list_checkout` "
          . "(`invoice_id`, `barang_id`, `jumlah_barang`) "
          . "VALUES $values;";
        $query5 = $this->db->query($sql5);
        if ($query5) {
          $strpos = strpos($input->expedisi, "Rp");
          $expedisi = substr($input->expedisi, 0, $strpos);
          $sql6 = "INSERT INTO `list_pengiriman` "
            . "(`invoice_id`, `expedisi`, `alamat_tujuan`, `harga_ongkir`) "
            . "VALUES (?, ?, ?, ?);";
          $query6 = $this->db->query($sql6, [$invoice_id, $expedisi, $input->alamat_tujuan, $input->harga_ongkir]);
          if (!$query6) {
            $error = TRUE;
            // $response->message = $this->db->last_query();
            $response->message = "Query 6 failed!";
          }
        } else {
          $error = TRUE;
          // $response->message = $this->db->last_query();
          $response->message = "Query 5 failed!";
        }
      }
    } else {
      $error = TRUE;
      // $response->message = $this->db->last_query();
      $response->message = "Query 2 failed!";
    }

    if ($error === FALSE) {
      $this->db->trans_commit();
      $response->success = TRUE;
      $response->message = "Pembuatan Invoice berhasil!";
      $this->cart->destroy();
    } else {
      $this->db->trans_rollback();
    }
    // $response->message = $this->db->error();
    // $response->message = "<pre>".(string)$this->db->error()."</pre>";
    return $response;
  }

  public function get_invoice_detail($invoice_id)
  {
    $response = new stdClass();
    $sql = "SELECT * FROM `list_invoice` li "
      .  "JOIN `list_invoice_status` lis ON lis.`invoice_status_id` = li.`invoice_status_id` "
      .  "WHERE li.`invoice_id` = ?;";
    $query = $this->db->query($sql, [$invoice_id]);
    if ($query) {
      $response->invoice = $query->row();
      $sql2 = "SELECT * FROM `list_checkout` lc "
        . "JOIN `list_barang` lb ON lb.`barang_id` = lc.`barang_id` "
        . "WHERE lc.`invoice_id` = ?;";
      $query2 = $this->db->query($sql2, [$invoice_id]);
      if ($query2) {
        $response->barang = $query2->result();
        $sql3 = "SELECT * FROM `list_pengiriman` "
          . "WHERE `invoice_id` = ?;";
        $query3 = $this->db->query($sql3, [$invoice_id]);
        if ($query3) {
          $response->pengiriman = $query3->row();
          $sql4 = "SELECT * FROM `settings`;";
          $query4 = $this->db->query($sql4);
          if ($query4) {
            $response->pengiriman_dari = $query4->row();
            $response->success = TRUE;
          }
        } else {
          $response->message = "Query 3 failed!";
        }
      } else {
        $response->message = "Query 2 failed!";
      }
    } else {
      $response->message = "Query 1 failed!";
    }
    return $response;
  }

  public function check_duplicate($table, $field, $value)
  {
    $sql = "SELECT `$field` FROM `$table` "
      .  "WHERE `$field` = {$this->db->escape_str($value)};";
    $query = $this->db->query($sql);
    if ($query) {
      if ($query->num_rows() > 0) {
        return FALSE;
      } else {
        return TRUE;
      }
    }
  }

  public function cek_cart($barang_id)
  {
    $response = new stdClass();
    $user = $this->session->user_id;
    $sql = "SELECT * FROM `list_cart` "
      .  "WHERE `user_id` = ? AND `barang_id` = ?;";
    $query = $this->db->query($sql, [$user, $barang_id]);
    if ($query) {
      $response->success = TRUE;
      $response->total = $query->num_rows();
      $response->data = $query->row();
    } else {
      $response->success = FALSE;
    }
    return $response;
  }

  public function get_cart_user()
  {
    $response = new stdClass();
    $user = $this->session->user_id;
    $sql = "SELECT lc.*, lb.* FROM `list_cart` lc "
      .  "JOIN `list_user` lu ON lu.`user_id` = lc.`user_id` "
      .  "JOIN `list_barang` lb ON lb.`barang_id` = lc.`barang_id` "
      .  "WHERE lc.`user_id` = ?;";
    $query = $this->db->query($sql, [$user]);
    if ($query) {
      $response->success = TRUE;
      $response->data = $query->result();
      $response->total = $query->num_rows();
    } else {
      $response->debug = $this->db->last_query();
    }
    return $response;
  }

  public function get_invoice_listed()
  {
    $response = new stdClass();
    $user = $this->session->user_id;
    $sql = "SELECT * FROM `list_invoice` li "
      .  "JOIN `list_invoice_status` lis ON lis.`invoice_status_id` = li.`invoice_status_id` "
      .  "WHERE li.`user_id` = ?;";
    $query = $this->db->query($sql, [$user]);
    if ($query) {
      $response->success = TRUE;
      $response->data = $query->result();
    } else {
      $response->message = "Query failed!";
    }
    return $response;
  }

  public function get_pesanan_masuk($start_date, $end_date)
  {
    $query = $this->db->select("li.`invoice_id`, li.`invoice_number`, li.`created_date`, li.`tanggal_jatuh_tempo`, li.`total_bayar`, li.`tanggal_bayar`, lis.`invoice_status`, lu.`nama`")
                      ->from("`list_invoice` li")
                      ->join("`list_invoice_status` lis", "lis.`invoice_status_id` = li.`invoice_status_id`")
                      ->join("`list_user` lu", "lu.`user_id` = li.`user_id`", "left")
                      ->where("li.`created_date` BETWEEN {$this->db->escape($start_date)} AND {$this->db->escape($end_date)}")
                      ->order_by("li.`created_date`", "DESC")
                      ->get();
    if ($query) {
      // var_dump($this->db->last_query());
      return $query->result();
    } else {
      return FALSE;
    }
  }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route["index"] = "home";
$route["kategori"] = "home/kategori";
$route["detail_barang"] = "home/view_detail_barang";
$route["all_barang"] = "home/all_barang";
$route["register"] = "auth/view_register_page";

$route["result_cart"] = "home/view_cart";
$route["add_cart"] = "home/add_cart";
$route["keranjang"] = "home/view_keranjang";
$route["update_cart"] = "home/update_cart";
$route["delete_cart"] = "home/delete_cart";
$route["clear_cart"] = "home/clear_cart";

$route["checkout"] = "checkout/view_checkout_page";
$route["create_invoice"] = "checkout/process_invoice_create";
$route["invoice"] = "checkout/view_invoice_page";
$route["pesanan_saya"] = "checkout/view_pesanan_saya";

$route["login"] = "auth";
$route["logout"] = "auth/logout";

$route["dashboard"] = "super_admin/view_dashboard_management";

$route["menu"] = "super_admin/view_menu_management";
$route["add_menu"] = "super_admin/process_add_menu";
$route["view_edit_menu"] = "super_admin/view_menu_edit";
$route["update_menu"] = "super_admin/process_menu_edit";
$route["delete_menu/(.+)"] = "super_admin/process_menu_delete/$1";

$route["add_access"] = "super_admin/process_access_add";
$route["view_access_edit"] = "super_admin/view_access_edit";
$route["update_access"] = "super_admin/process_access_edit";
$route["delete_access/(.+)"] = "super_admin/process_access_delete/$1";

$route["sub_menu"] = "super_admin/view_submenu_management";
$route["add_sub"] = "super_admin/process_submenu_add";
$route["view_submenu_edit"] = "super_admin/view_submenu_edit";
$route["update_sub"] = "super_admin/process_submenu_edit";
$route["delete_sub/(.+)"] = "super_admin/process_submenu_delete/$1";

$route["admin"] = "super_admin/view_admin_management";
$route["add_admin"] = "super_admin/validate_admin_add";
$route["admin_detail"] = "super_admin/view_admin_detail";
$route["edit_admin"] = "super_admin/validate_admin_edit";
$route["delete_admin/(.+)"] = "super_admin/process_admin_delete/$1";

$route["settings"] = "settings";
$route["get_provinsi"] = "ongkir/provinsi";
$route["get_city"] = "ongkir/kota";
$route["get_expedisi"] = "ongkir/expedisi";
$route["get_services"] = "ongkir/services";
$route["update_settings"] = "settings/validate_settings_edit";

$route["user"] = "admin/view_user_management";
$route["add_user"] = "admin/process_user_add";
$route["view_user_edit"] = "admin/view_user_edit";
$route["edit_user"] = "admin/process_user_edit";
$route["delete_user/(.+)"] = "admin/process_user_delete/$1";

$route["kategori_barang"] = "barang/view_kategori_barang";
$route["add_kategori"] = "barang/validate_kategori_add";
$route["view_kategori"] = "barang/view_kategori_edit";
$route["edit_kategori"] = "barang/validate_kategori_edit";
$route["delete_kategori/(.+)"] = "barang/process_kategori_delete/$1";

$route["barang"] = "barang/view_barang_management";
$route["add_barang"] = "barang/validate_barang_add";
$route["barang_edit"] = "barang/view_barang_edit";
$route["edit_barang"] = "barang/validate_barang_edit";
$route["delete_barang/(.+)"] = "barang/process_barang_delete/$1";
$route["detail_barang/(.+)"] = "barang/view_barang_detail/$1";
$route["view_gambar_barang"] = "barang/view_gambar_barang";
$route["save_gambar"] = "barang/process_gambar_barang_add";
$route["delete_gambar"] = "barang/process_gambar_barang_delete";

$route["pesanan_masuk"] = "pesanan/view_pesanan_masuk_management";
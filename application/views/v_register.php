<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Register - SansShop</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?= base_url("template/") ?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Sweet Alert -->
  <link rel="stylesheet" href="<?= base_url("template/") ?>plugins/sweetalert2/sweetalert2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url("template/") ?>dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Sweet -->
  <script src="<?= base_url("template/") ?>plugins/sweetalert2/sweetalert2.all.min.js"></script>
  <script>
    function sweet(icon, title, text) {
      Swal.fire({
        icon: icon,
        title: title,
        text: text
      })
    }
  </script>
</head>

<body style="background-color: #e9ecef;">
  <?= $this->session->flashdata('pesan'); ?>
  <div class="container">
    <div class="row mt-5">
      <div class="col-md-6 mx-auto">
        <div class="card">
          <div class="card-body">
            <h4 class="text-center">SansShop Register</h4>
            <?= form_open("register") ?>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Nama lengkap</label>
                  <input type="text" class="form-control" name="nama" value="<?php echo set_value("nama"); ?>" placeholder="Masukan nama lengkap anda..." required>
                  <?= form_error("nama", "<small class='text-danger'>", "</small>"); ?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" class="form-control" name="username" value="<?php echo set_value("username"); ?>" placeholder="Masukan username anda..." required>
                  <?= form_error("username", "<small class='text-danger'>", "</small>"); ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" class="form-control" name="email" value="<?php echo set_value("email"); ?>" placeholder="Masukan email anda..." required>
                  <?= form_error("email", "<small class='text-danger'>", "</small>"); ?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>No. HP</label>
                  <input type="text" class="form-control" name="phone" value="<?php echo set_value("phone"); ?>" placeholder="Masukan nomor hp anda..." required>
                  <?= form_error("phone", "<small class='text-danger'>", "</small>"); ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="password" class="form-control" placeholder="Masukan password anda..." required>
                  <?= form_error("password", "<small class='text-danger'>", "</small>"); ?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Ulang password</label>
                  <input type="password" name="re_password" class="form-control" placeholder="Masukan ulang password anda..." required>
                  <?= form_error("re_password", "<small class='text-danger'>", "</small>"); ?>
                </div>
              </div>
            </div>
            <?= form_submit("", "Register", "class='btn btn-primary'") ?>
            <?= form_close(); ?>
          </div>
          <div class="card-footer">
            <p>Sudah punya akun? <a href="<?= base_url("login"); ?>">Login disni</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- jQuery -->
  <script src="<?= base_url("template/") ?>plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="<?= base_url("template/") ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url("template/") ?>dist/js/adminlte.min.js"></script>
</body>

</html>
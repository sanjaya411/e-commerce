<!-- Humberger Begin -->
<div class="humberger__menu__overlay"></div>
<div class="humberger__menu__wrapper">
  <div class="humberger__menu__cart">
    <ul>
      <li><a href="#" class="text-muted"><i class="fa fa-shopping-cart"></i><span id="resultCartMobile">3</span></a></li>
    </ul>
    <div class="header__cart__price font-weight-bold">Total: Rp. <span id="totalPriceMobile"></span></div>
  </div>
  <div class="humberger__menu__widget">
    <div class="header__top__right__auth">
      <?php
      if ($this->session->login === TRUE) {
        echo "<a href='".base_url("profile_saya")."'><i class='fas fa-user'></i> My Profile</a>";
      } else {
        echo "<a href='" . base_url("login") . "'><i class='fas fa-sign-in-alt'></i> Login</a>";
      }
      ?>
    </div>
  </div>
  <nav class="humberger__menu__nav mobile-menu">
    <ul>
      <li class="active"><a href="<?= base_url(); ?>">Home</a></li>
      <li><a href="<?= base_url("all_barang?page=1") ?>">Shop</a></li>
      <li><a href="<?= base_url("contact") ?>">Contact</a></li>
    </ul>
  </nav>
  <div id="mobile-menu-wrap"></div>
  <div class="header__top__right__social">
    <a href="#"><i class="fab fa-facebook"></i></a>
    <a href="#"><i class="fab fa-twitter"></i></a>
    <a href="#"><i class="fab fa-linkedin"></i></a>
    <a href="#"><i class="fab fa-pinterest-p"></i></a>
  </div>
  <div class="humberger__menu__contact">
    <ul>
      <li><i class="fa fa-envelope"></i> hello@colorlib.com</li>
      <li>Free Shipping for all Order of $99</li>
    </ul>
  </div>
</div>
<!-- Humberger End -->

<!-- Header Section Begin -->
<header class="header">
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <div class="header__logo">
          <a href="<?= base_url(); ?>">
            <h3>SansHop</h3>
          </a>
        </div>
      </div>
      <div class="col-lg-6">
        <nav class="header__menu">
          <ul>
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li><a href="<?= base_url("all_barang?page=1") ?>">Shop</a></li>
            <li><a href="<?= base_url("contact") ?>">Contact</a></li>
          </ul>
        </nav>
      </div>
      <div class="col-lg-3">
        <div class="header__cart">
          <ul>
            <li><a href="<?= base_url("keranjang"); ?>" class="text-muted"><i class="fas fa-shopping-cart"></i><span id="resultCart">3</span></a></li>
            <?php if ($this->session->login === TRUE) { ?>
              <li><a href="<?= base_url("profile_saya") ?>" class="text-muted"><i class="fas fa-user mr-2"></i>My Profile</a></li>
            <?php } else { ?>
              <li><a href="<?= base_url("login") ?>" class="text-muted"><i class="fas fa-sign-in-alt mr-2"></i>Login</a></li>
            <?php } ?>
          </ul>
          <div class="header__cart__price font-weight-bold">Total: Rp. <span id="totalCart"></span></div>
        </div>
      </div>
      <div class="humberger__open">
        <i class="fas fa-bars"></i>
      </div>
    </div>
</header>
<!-- Header Section End -->

<!-- Hero Section Begin -->
<section class="hero">
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <div class="hero__categories">
          <div class="hero__categories__all">
            <i class="fa fa-bars text-white"></i>
            <span>All category</span>
          </div>
          <ul <?php echo $title != "Home" ? "style='display: none;'" : ""; ?>>
            <?php foreach ($kategori as $item) {
              echo "<li><a href='" . base_url("kategori?id=" . encrypt_url($item->kategori_id)) . "&page=1'>$item->nama</a></li>";
            } ?>
          </ul>
        </div>
      </div>
      <div class="col-lg-9">
        <div class="hero__search">
          <div class="hero__search__form">
            <form action="#">
              <input type="text" placeholder="What do yo u need?">
              <button type="submit" class="site-btn">SEARCH</button>
            </form>
          </div>
          <div class="hero__search__phone">
            <div class="hero__search__phone__icon">
              <i class="fa fa-phone"></i>
            </div>
            <div class="hero__search__phone__text">
              <h5>+62 812 1349 0755</h5>
              <span>support 24/7 time</span>
            </div>
          </div>
        </div>
        <?php if ($title == "Home") { ?>
          <div class="hero__item set-bg" data-setbg="<?= base_url("assets/img/gambar2.jpeg") ?>">
            <div class="hero__text text-white">
              <h2 class="text-white">Brand <br />100% Original</h2>
              <p>Free Pickup and Delivery Available</p>
              <a href="#" class="primary-btn">SHOP NOW</a>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</section>
<!-- Hero Section End -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <a href="index3.html" class="brand-link">
    <img src="<?= base_url("template/") ?>dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">Dashboard Admin</span>
  </a>

  <div class="sidebar">
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="<?= base_url("template/") ?>dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="<?= base_url("admin_profile"); ?>" class="d-block"><?= $user_info->nama; ?></a>
      </div>
    </div>

    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <?php foreach ($list_menu as $item) { ?>
        <li class="nav-header"><?= $item->menu; ?></li>
        <?php
          $list_sub = $this->db->select("*")->from("list_sub_menu")->where("menu_id", $item->menu_id)->get()->result();
          foreach($list_sub as $sub) {
        ?>
        <li class="nav-item">
          <a href="<?= base_url($sub->link); ?>" class="nav-link <?= $title_admin == $sub->header ? "active" : ""; ?>">
            <i class="<?= $sub->icon ?>"></i>
            <p><?= $sub->title; ?></p>
          </a>
        </li>
        <?php } } ?>
        <li class="nav-item">
          <a href="<?= base_url("logout"); ?>" class="nav-link">
            <i class="nav-icon fas fa-sign-out-alt"></i>
            <p>Logout</p>
          </a>
        </li>
      </ul>
    </nav>
  </div>
</aside>

<div class="content-wrapper" style="min-height: 540px;">
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><?= $title_admin; ?></h1>
        </div>
      </div>
    </div>
  </div>
  <div class="content">
    <div class="container-fluid">
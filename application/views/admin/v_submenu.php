<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <button class="btn btn-success btn-sm mb-3" data-toggle="modal" data-target="#addSub">Tambah Submenu</button>
        <?= $this->session->flashdata('pesan'); ?>
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="data">
            <thead>
              <tr>
                <th>No</th>
                <th>Menu</th>
                <th>Title</th>
                <th>Ikon</th>
                <th>link</th>
                <th>Title Page</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (isset($list_sub)) {
                $index = 1;
                foreach ($list_sub as $item) {
                  echo "<tr>";
                  echo "<td>$index</td>";
                  echo "<td>$item->menu</td>";
                  echo "<td>$item->title</td>";
                  echo "<td>$item->icon</td>";
                  echo "<td>$item->link</td>";
                  echo "<td>$item->header</td>";
                  echo "<td>";
                  echo "<button onclick='editSub($item->sub_id)' tooltip='true' title='Edit' class='btn btn-info btn-sm mr-2'  data-toggle='modal' data-target='#editSub'><i class='fas fa-edit'></i></button>";
                  echo "<a href='" . base_url('delete_sub/' . $item->sub_id) . "' tooltip='true' title='Hapus' class='btn btn-danger btn-sm mr-2'><i class='fas fa-trash'></i></a>";
                  echo "</td>";
                  echo "</tr>";
                  $index++;
                }
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addSub">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Submenu</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open("add_sub"); ?>
        <div class="form-group">
          <?= form_label("Menu"); ?>
          <select name="menu_id" class="form-control" required>
            <option disabled selected>-- Pilih menu --</option>
            <?php
            foreach ($menu->result() as $item) {
              echo "<option value='$item->menu_id'>$item->menu</option>";
            }
            ?>
          </select>
        </div>
        <div class="form-group">
          <?= form_label("Title"); ?>
          <?= form_input("title", "", "class='form-control' placeholder='Masukan title...' required"); ?>
        </div>
        <div class="form-group">
          <?= form_label("Ikon"); ?>
          <?= form_input("icon", "", "class='form-control' placeholder='Masukan ikon...' required"); ?>
        </div>
        <div class="form-group">
          <?= form_label("Link"); ?>
          <?= form_input("link", "", "class='form-control' placeholder='Masukan link...' required"); ?>
        </div>
        <div class="form-group">
          <?= form_label("Title Page"); ?>
          <?= form_input("header", "", "class='form-control' placeholder='Masukan title header...' required"); ?>
        </div>
        <?= form_submit("submit", "Simpan", "class='btn btn-success btn-sm'") ?>
        <?= form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editSub">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Submenu</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open("update_sub"); ?>
        <div class="form-group">
          <?= form_label("Menu"); ?>
          <?= form_hidden("sub_id", ""); ?>
          <select name="menu_id" id="menuId" class="form-control" required>
            <option disabled selected>-- Pilih menu --</option>
            <?php
            foreach ($menu->result() as $item) {
              echo "<option value='$item->menu_id'>$item->menu</option>";
            }
            ?>
          </select>
        </div>
        <div class="form-group">
          <?= form_label("Title"); ?>
          <?= form_input("title", "", "class='form-control' placeholder='Masukan title...' required"); ?>
        </div>
        <div class="form-group">
          <?= form_label("Ikon"); ?>
          <?= form_input("icon", "", "class='form-control' placeholder='Masukan ikon...' required"); ?>
        </div>
        <div class="form-group">
          <?= form_label("Link"); ?>
          <?= form_input("link", "", "class='form-control' placeholder='Masukan link...' required"); ?>
        </div>
        <div class="form-group">
          <?= form_label("Title Page"); ?>
          <?= form_input("header", "", "class='form-control' placeholder='Masukan title header...' required"); ?>
        </div>
        <?= form_submit("submit", "Simpan", "class='btn btn-success btn-sm'") ?>
        <?= form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
  function editSub(subId) {
    $.ajax({
      url: "<?= base_url("view_submenu_edit?id=") ?>"+subId,
      type: "GET",
      cache: false,
      success: function(result) {
        let hasil = JSON.parse(result);
        let menu = document.getElementById("menuId");
        for (i = 0; i < menu.length; i++) {
          if (menu[i].value == hasil.data.menu_id) {
            menu[i].setAttribute("selected", "true");
          }
        }
        $("[name='sub_id']").val(hasil.data.sub_id);      
        $("[name='title']").val(hasil.data.title);      
        $("[name='icon']").val(hasil.data.icon);        
        $("[name='link']").val(hasil.data.link);        
        $("[name='header']").val(hasil.data.header);
      }
    });
  }
</script>
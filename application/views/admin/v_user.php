<div class="col-md-12">
  <div class="card">
    <div class="card-header">
      <h5>Data User</h5>
    </div>
    <div class="card-body">
      <button class="btn btn-success btn-sm mb-3" data-toggle="modal" data-target="#addUser">Add Data</button>
      <?= $this->session->flashdata('pesan'); ?>
      <table class="table table-bordered table-hover" id="data">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Username</th>
            <th>Level</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $index = 1;
          foreach ($list_user->result() as $item) {
            echo "<tr>";
            echo "<td>$index</td>";
            echo "<td>$item->nama</td>";
            echo "<td>$item->username</td>";
            if ($item->role == "Super Admin") {
              echo "<td><div class='badge badge-success'>$item->role</div></td>";
            } else if ($item->role == "Admin") {
              echo "<td><div class='badge badge-primary'>$item->role</div></td>";
            } else {
              echo "<td><div class='badge badge-danger'>$item->role</div></td>";
            }
            echo "<td>";
            echo "<button onclick=\"editUser('view_user_edit?id=$item->user_id', 'primaryForm')\" tooltip='true' title='Edit' data-toggle='modal' data-target='#editUser' class='btn btn-info btn-sm mr-2'><i class='fas fa-edit'></i></button>";
            echo "<button onclick=\"deletePrompt('delete_user/$item->user_id')\" tooltip='true' title='Hapus' class='btn btn-danger btn-sm'><i class='fas fa-trash'></i></button>";
            echo "</td>";
            echo "</tr>";
            $index++;
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal fade" id="addUser">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <?= form_open("add_user"); ?>
        <div class="form-group">
          <label>Nama User</label>
          <input type="text" name="nama" class="form-control" placeholder="Masukan nama user..." required>
        </div>
        <div class="form-group">
          <label>Email</label>
          <input type="email" name="email" class="form-control" placeholder="Masukan email....">
        </div>
        <div class="form-group">
          <label>Username</label>
          <input type="text" class="form-control" name="username" placeholder="Masukan username..." required>
        </div>
        <div class="form-group">
          <label>Password</label>
          <input type="password" class="form-control" name="password" placeholder="Masukan password..." required>
        </div>
        <?= form_submit("submit", "Simpan", "class='btn btn-success btn-sm' id='addUser'"); ?>
        <?= form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editUser">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <?= form_open("edit_user", "id='primaryForm'"); ?>
        <div class="form-group">
          <label>Nama User</label>
          <input type="hidden" name="user_id">
          <input type="text" name="nama" class="form-control" placeholder="Masukan nama user..." required>
        </div>
        <div class="form-group">
          <label>Email</label>
          <input type="email" name="email" class="form-control" placeholder="Masukan email....">
        </div>
        <div class="form-group">
          <label>Username</label>
          <input type="text" class="form-control" name="username" placeholder="Masukan username..." required>
        </div>
        <div class="form-group">
          <label>Password</label>
          <input type="password" class="form-control" name="password" placeholder="Masukan password...">
        </div>
        <button type="submit" class="btn btn-success btn-sm">Update User</button>
        <?= form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
var base_url = "http://localhost/CI/web_olshop/";

function editUser(url, formId) {
  $.ajax({
    url: url,
    type: "GET",
    cache: false,
    success: function(result) {
      let hasil = JSON.parse(result);
      console.log(hasil);
      $("[name='user_id']").val(hasil.data.user_id);
      $("[name='nama']").val(hasil.data.nama);
      $("[name='email']").val(hasil.data.email);
      $("[name='username']").val(hasil.data.username);
      $("[name='role_id']").val(hasil.data.role_id);
    }
  })
}
</script>
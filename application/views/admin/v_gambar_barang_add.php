<?php echo $this->session->flashdata('pesan'); ?>
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <?php echo form_open_multipart("edit_barang") ?>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?php echo form_label("Nama Barang") ?>
              <?php echo form_input("nama", $barang->nama, "class='form-control' placeholder='Masukan nama barang...' required"); ?>
              <?php echo form_hidden("barang_id", $barang->barang_id); ?>
            </div>
            <div class="form-group">
              <?php echo form_label("Kategori"); ?>
              <select name="kategori_id" class="form-control">
                <?php
                foreach ($list_kategori as $item) {
                  if ($barang->kategori_id == $item->kategori_id) {
                    echo "<option selected value='$item->kategori_id'>$item->nama</option>";
                  } else {
                    echo "<option value='$item->kategori_id'>$item->nama</option>";
                  }
                }
                ?>
              </select>
            </div>
            <div class="form-group">
              <?php echo form_label("Stok"); ?>
              <?php echo form_input("stok", $barang->stok, "class='form-control' placeholder='Masukan stok barang...' required"); ?>
            </div>
            <div class="form-group">
              <?php echo form_label("Berat (Gram)"); ?>
              <?php echo form_input(["name" => "berat", "type" => "number"], $barang->berat, "class='form-control' placeholder='Masukan stok barang...' required") ?>
            </div>
            <div class="form-group">
              <?php echo form_label("Harga"); ?>
              <?php echo form_input("harga", $barang->harga, "class='form-control' placeholder='Masukan harga barang...' required") ?>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <?php echo form_label("Description"); ?>
              <?php echo form_textarea("description", $barang->description, "class='form-control' placeholder='Masukan deskripsi barang...' required") ?>
            </div>
            <div class="form-group">
              <img class="d-block mx-auto" style="width: 80%; height: 200px;" id="gambarLoadEdit" src="<?php echo base_url("assets/img/" . $barang->foto) ?>">
            </div>
            <div class="form-group">
              <?php echo form_label("Foto Cover"); ?>
              <input type="file" name="foto" class="form-control" id="previewGambarEdit">
            </div>
          </div>
        </div>
        <h4 class="mb-2">Gambar Lainnya</h4>
        <div class="row mb-3">
          <div class="col-md-4">
            <div class="form-group">
              <input type="file" class="form-control" id="gambarLain">
            </div>
          </div>
        </div>
        <div class="row mb-5" id="listGambar">
        </div>
        <?php echo form_submit("Simpan", "Simpan", "class='btn btn-success btn-sm'") ?>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>

<div class="loading-container d-none" id="loading">
  <h4>Mohon Tunggu...</h4>
</div>

<script>
  var barangId = <?php echo $barang->barang_id; ?>;
  listGambar(barangId);

  function readerImageEdit(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $("#gambarLoadEdit").attr("src", e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#previewGambarEdit").change(function() {
    readerImageEdit(this)
  })

  $("#gambarLain").change(function() {
    $("#loading").removeClass("d-none");
    $("#loading").addClass("d-flex");
    var file_data = $(this).prop('files')[0];
    var form_data = new FormData();
    var ext = $(this).val().split('.').pop().toLowerCase();
    if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
      alert("only jpg and png images allowed");
      return;
    }
    var picsize = (file_data.size);
    console.log(picsize); /*in byte*/
    if (picsize > 5097152) /* 5mb*/ {
      alert("Image allowd less than 5 mb")
      return;
    }
    form_data.append('file', file_data);
    $.ajax({
      url: '<?php echo base_url("save_gambar?id=") ?>' + barangId,
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      success: function(result) {
        let hasil = JSON.parse;
        listGambar(barangId);
        $("#loading").addClass("d-none");
        $("#loading").removeClass("d-flex");
      },
      error: function() {
        $("#loading").addClass("d-none");
        $("#loading").removeClass("d-flex");
        alert("Gagal!");
      }
    });
  });

  function listGambar(barangId) {
    $.ajax({
      url: "<?php echo base_url("view_gambar_barang?id=") ?>" + barangId,
      type: "GET",
      cache: false,
      success: function(result) {
        let hasil = JSON.parse(result);
        $("#listGambar").html(hasil.html);
      }
    });
  }
</script>
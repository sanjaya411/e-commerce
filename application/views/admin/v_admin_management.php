<?php echo $this->session->flashdata('pesan'); ?>
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <button class="btn btn-success btn-sm mb-3" data-toggle="modal" data-target="#addAdmin">Tambah Admin</button>
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="data">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Username</th>
                <th>Email</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $index = 1;
              foreach ($list_admin as $item) {
                echo "<tr>";
                echo "<td>$index</td>";
                echo "<td>$item->nama</td>";
                echo "<td>$item->username</td>";
                echo "<td>$item->email</td>";
                echo "<td>";
                echo "<button class='btn btn-info btn-sm mx-1' tooltip='true' title='Edit' data-toggle='modal' data-target='#editAdmin' onclick='editAdmin($item->user_id)'><i class='fas fa-edit'></i></button>";
                echo "<button onclick=\"deletePrompt('delete_admin/$item->user_id')\" class='btn btn-danger btn-sm mx-1' tooltip='true' title='Hapus'><i class='fas fa-trash'></i></button>";
                echo "</td>";
                echo "</tr>";
                $index++;
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addAdmin">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Tambah Admin</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?php echo form_open("add_admin"); ?>
        <div class="form-group">
          <?php echo form_label("Nama") ?>
          <?php echo form_input("nama", "", "class='form-control' placeholder='Masukan nama...' required"); ?>
        </div>
        <div class="form-group">
          <?php echo form_label("Email") ?>
          <?php echo form_input("email", "", "class='form-control' placeholder='Masukan email...' required"); ?>
        </div>
        <div class="form-group">
          <?php echo form_label("Username") ?>
          <?php echo form_input("username", "", "class='form-control' placeholder='Masukan email...' required"); ?>
        </div>
        <div class="form-group">
          <?php echo form_label("Password") ?>
          <?php echo form_password("password", "", "class='form-control' placeholder='Masukan password...' required"); ?>
        </div>
        <?php echo form_submit("Submit", "Simpan", "class='btn btn-success btn-sm'");  ?>
        <?php echo form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="editAdmin">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Edit Admin</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?php echo form_open("edit_admin"); ?>
        <div class="form-group">
          <?php echo form_label("Nama") ?>
          <?php echo form_hidden("user_id", ""); ?>
          <?php echo form_input("nama", "", "class='form-control' placeholder='Masukan nama...' required"); ?>
        </div>
        <div class="form-group">
          <?php echo form_label("Email") ?>
          <?php echo form_input("email", "", "class='form-control' placeholder='Masukan email...' required"); ?>
        </div>
        <div class="form-group">
          <?php echo form_label("Username") ?>
          <?php echo form_input("username", "", "class='form-control' placeholder='Masukan email...' required"); ?>
        </div>
        <div class="form-group">
          <?php echo form_label("Password") ?>
          <?php echo form_password("password", "", "class='form-control' placeholder='Masukan password...' required"); ?>
        </div>
        <?php echo form_submit("Submit", "Simpan", "class='btn btn-success btn-sm'");  ?>
        <?php echo form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
function editAdmin(userId) {
  $.ajax({
    url: "<?php echo base_url('admin_detail?id=') ?>" + userId,
    type: "GET",
    cache: false,
    success: function(result) {
      let hasil = JSON.parse(result);
      $("[name='user_id']").val(hasil.data.user_id);
      $("[name='nama']").val(hasil.data.nama);
      $("[name='username']").val(hasil.data.username);
      $("[name='email']").val(hasil.data.email);
    }
  })
}

function confirmDelete(userId) {

}
</script>
<?= $this->session->flashdata('pesan'); ?>
<div class="row">
  <div class="col">
    <div class="card">
      <div class="card-body">
        <div class="row mb-4">
          <div class="col-md-8 mx-auto">
            <div class="form-group">
              <label>Filter Pesanan</label>
              <div class="row">
                <div class="col-md-6">
                  <input type="date" name="start_date" class="form-control" id="startDate" value="<?= $start_date ?>">
                </div>
                <div class="col-md-6">
                  <input type="date" name="end_date" class="form-control" id="endDate" value="<?= $end_date ?>">
                </div>
              </div>
            </div>
            <button class="btn btn-outline-success btn-sm" onclick="filterPesanan()">Cari</button>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-hover table-bordered" id="data">
            <thead>
              <tr>
                <th>No</th>
                <th>No Invoice</th>
                <th>User</th>
                <th>Created Date</th>
                <th>Jatuh Tempo</th>
                <th>Total Bayar</th>
                <th>Tanggal Bayar</th>
                <th>Status Pesanan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $index = 1;
              foreach ($list_pesanan as $item) {
                echo "<tr>";
                echo "<td>$index</td>";
                echo "<td>$item->invoice_number</td>";
                if ($item->nama == NULL) {
                  echo "<td>User</td>";
                } else {
                  echo "<td>$item->nama</td>";
                }
                echo "<td>" . date("d M Y H:i", strtotime($item->created_date)) . "</td>";
                if ($item->tanggal_jatuh_tempo != NULL) {
                  echo "<td>" . date("d M Y H:i", strtotime($item->tanggal_jatuh_tempo)) . "</td>";
                } else {
                  echo "<td>-</td>";
                }
                echo "<td>Rp. " . number_format($item->total_bayar, "0", ",", ".") . ",-</td>";
                if ($item->tanggal_bayar != NULL) {
                  echo "<td>" . date("d M Y H:i", strtotime($item->tanggal_bayar)) . "</td>";
                } else {
                  echo "<td>-</td>";
                }
                echo "<td>$item->invoice_status</td>";
                echo "<td>";
                echo "<a href='" . base_url("detail_pesanan/" . $item->invoice_id) . "' class='btn btn-primary btn-sm mx-1' tooltip='true' title='Detail'><i class='fas fa-eye'></i></a>";
                echo "</td>";
                echo "</tr>";
                $index++;
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function filterPesanan() {
    var startDate  = $("#startDate").val();
    var endDate = $("#endDate").val();

    window.location.href = `<?= base_url("pesanan_masuk") ?>?start_date=${startDate}&end_date=${endDate}`;
  }
</script>
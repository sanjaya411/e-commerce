<?php echo $this->session->flashdata('pesan'); ?>
<div class="row">
  <div class="col-md-8">
    <div class="card">
      <div class="card-body">
      <h4>Identitas Toko Anda</h4>
        <?php echo form_open("update_settings"); ?>
          <div class="form-group">
            <label>Nama Toko</label>
            <input type="text" name="nama_toko" class="form-control" placeholder="Masukan nama toko" value="<?php echo $setting->nama_toko ?>" required>
          </div>
          <div class="form-group">
            <label>Nomor HP</label>
            <input type="number" name="no_hp" class="form-control" placeholder="Masukan nama toko" value="<?php echo $setting->no_hp; ?>" required>
          </div>
          <div class="form-group">
            <label>Provinsi</label>
            <select name="provinsi_id" class="form-control" id="provinsi" required></select>
          </div>
          <div class="form-group">
            <label>Kota</label>
            <select name="kota_id" class="form-control" required></select>
          </div>
          <div class="form-group">
            <label>Alamat Detail</label>
            <textarea class="form-control" name="alamat_toko" rows="3" placeholder="Masukan alamat detail toko anda" required><?php echo $setting->alamat_toko; ?></textarea>
          </div>
          <?php echo form_submit("", "Save", "class='btn btn-success'"); ?>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $.ajax({
      url: "<?php echo base_url("get_provinsi") ?>",
      type: "POST",
      cache: false,
      success: function(result) {
        $("[name='provinsi_id']").html(result);
        // console.log(result);
      }
    });

    $("#provinsi").change(function() {
      var provinsiId = $("#provinsi").val();
      $.ajax({
        url: "<?php echo base_url("get_city") ?>",
        type: "POST",
        cache: false,
        data: {
          provinsi_id: provinsiId
        },
        success: function(result) {
          $("[name='kota_id']").html(result);
          // console.log(result);
        }
      });
    });
  });
</script>
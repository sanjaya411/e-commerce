<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h5>Menu Management</h5>
      </div>
      <div class="card-body">
        <button class="btn btn-success btn-sm mb-3" data-toggle="modal" data-target="#addMenu">Tambah Menu</button>
        <?= $this->session->flashdata("pesan"); ?>
        <table class="table table-bordered table-hover" id="data">
          <thead>
            <tr>
              <th>No</th>
              <th>Menu</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $index = 1;
            if ($menu->num_rows() > 0) {
              foreach ($menu->result() as $item) {
                echo "<tr>";
                echo "<td>$index</td>";
                echo "<td>$item->menu</td>";
                echo "<td>";
                echo "<button onclick='editMenu($item->menu_id)' tooltip='true' title='Edit' class='btn btn-info btn-sm mr-2'  data-toggle='modal' data-target='#editMenu'><i class='fas fa-edit'></i></button>";
                echo "<a href='" . base_url('delete_menu/' . $item->menu_id) . "' class='btn btn-danger btn-sm' tooltip='true' title='Hapus'><i class='fas fa-trash'></i></a>";
                echo "</td>";
                echo "</tr>";
                $index++;
              }
            } else {
              echo "<tr>";
              echo "<td colspan='3'>Tidak ada menu list</td>";
              echo "</tr>";
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h5>Access Management</h5>
      </div>
      <div class="card-body">
        <button class="btn btn-success btn-sm mb-3" data-toggle="modal" data-target="#addAccess">Tambah Access</button>
        <?= $this->session->flashdata('pesan2'); ?>
        <table class="table table-bordered table-hover" id="data2">
          <thead>
            <tr>
              <th>No</th>
              <th>Menu</th>
              <th>Role</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (isset($list_access) && count($list_access) > 0) {
              $index = 1;
              foreach ($list_access as $item) {
                echo "<tr>";
                echo "<td>$index</td>";
                echo "<td>$item->menu</td>";
                echo "<td>$item->role</td>";
                echo "<td>";
                echo "<button onclick='editAccess($item->access_id)' tooltip='true' title='Edit' class='btn btn-info btn-sm mr-2'  data-toggle='modal' data-target='#editAccess'><i class='fas fa-edit'></i></button>";
                echo "<a href='" . base_url('delete_access/' . $item->access_id) . "' tooltip='true' title='Hapus' class='btn btn-danger btn-sm mr-2'><i class='fas fa-trash'></i></a>";
                echo "</td>";
                echo "</tr>";
                $index++;
              }
            } else {
              echo "<tr>";
              echo "<td colspan='4'>No access listed</td>";
              echo "</tr>";
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="addMenu">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Menu</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open("add_menu") ?>
        <div class="form-group">
          <label>Manu</label>
          <?= form_input("menu", "", "class='form-control'") ?>
        </div>
        <?= form_submit("submit", "Simpan", "class='btn btn-success btn-sm' autocomplete='off'"); ?>
        <?= form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editMenu">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Menu</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open("update_menu") ?>
        <div class="form-group">
          <label>Menu</label>
          <input type="hidden" name="menu_id" value="">
          <input type="text" name="menu_edit" class="form-control" value="">
        </div>
        <?= form_submit("submit", "Update", "class='btn btn-success btn-sm' autocomplete='off'"); ?>
        <?= form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addAccess">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Access</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open("add_access") ?>
        <div class="form-group">
          <label>Menu</label>
          <select name="menu_id" class="form-control">
            <option disabled selected>-- Pilih Menu --</option>
            <?php
            foreach ($menu->result() as $item) {
              echo "<option value='$item->menu_id'>$item->menu</option>";
            }
            ?>
          </select>
        </div>
        <div class="form-group">
          <select name="role_id" class="form-control">
            <option selected disabled>-- Pilih Role --</option>
            <?php
            foreach ($list_role->result() as $item) {
              echo "<option value='$item->role_id'>$item->role</option>";
            }
            ?>
          </select>
        </div>
        <?= form_submit("submit", "Simpan", "class='btn btn-success btn-sm' autocomplete='off'"); ?>
        <?= form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editAccess">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Access</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open("update_access") ?>
        <div class="form-group">
          <label>Menu</label>
          <input type="hidden" name="access_id" value="">
          <select name="menu_id" id="menuIdAccess" class="form-control">
            <option disabled selected>-- Pilih Menu --</option>
            <?php
            foreach ($menu->result() as $item) {
              echo "<option value='$item->menu_id'>$item->menu</option>";
            }
            ?>
          </select>
        </div>
        <div class="form-group">
          <select name="role_id" id="roleIdAccess" class="form-control">
            <option selected disabled>-- Pilih Role --</option>
            <?php
            foreach ($list_role->result() as $item) {
              echo "<option value='$item->role_id'>$item->role</option>";
            }
            ?>
          </select>
        </div>
        <?= form_submit("submit", "Simpan", "class='btn btn-success btn-sm' autocomplete='off'"); ?>
        <?= form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function editMenu(menuId) {
  $.ajax({
    url: "<?= base_url('view_edit_menu?id='); ?>" + menuId,
    type: "GET",
    cache: false,
    success: function(result) {
      let hasil = JSON.parse(result);
      if (hasil.success == 200) {
        $("[name='menu_edit']").val(hasil.data.menu);
        $("[name='menu_id']").val(hasil.data.menu_id);
      } else {
        alert("gagal!");
      }
    }
  });
}

function editAccess(accessId) {
  $.ajax({
    url: "<?= base_url('view_access_edit?id='); ?>" + accessId,
    type: "GET",
    cache: false,
    success: function(result) {
      let hasil = JSON.parse(result);
      $("[name='access_id']").val(hasil.data.access_id);
      $('[name="menu_id"]').val(hasil.data.menu_id);
      $('[name="role_id"]').val(hasil.data.role_id);
    }
  })
}
</script>
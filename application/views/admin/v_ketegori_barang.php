<?php echo $this->session->flashdata('pesan'); ?>
<div class="row">
  <div class="col-md-8">
    <div class="card">
      <div class="card-body">
        <button class="btn btn-success btn-sm mb-3" data-toggle="modal" data-target="#addKategori">Tambah
          Kategori</button>
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="data">
            <thead>
              <tr>
                <th>No</th>
                <th>Kategori</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $index = 1;
              foreach ($list_kategori as $item) {
                echo "<tr>";
                echo "<td>$index</td>";
                echo "<td>$item->nama</td>";
                if ($item->active == 1) {
                  echo "<td><span class='badge badge-primary'>Aktif</span></td>";
                } else {
                  echo "<td><span class='badge badge-danger'>Tidak Aktif</span></td>";
                }
                echo "<td>";
                echo "<button class='btn btn-info btn-sm mx-1' tooltip='true' title='Edit' data-toggle='modal' data-target='#editAdmin' onclick='editAdmin($item->kategori_id)'><i class='fas fa-edit'></i></button>";
                echo "<button onclick=\"deletePrompt('delete_kategori/$item->kategori_id')\" class='btn btn-danger btn-sm mx-1' tooltip='true' title='Hapus'><i class='fas fa-trash'></i></button>";
                echo "</td>";
                echo "</tr>";
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addKategori">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Tambah Kategori</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?php echo form_open("add_kategori"); ?>
        <div class="form-group">
          <?php echo form_label("Kategori") ?>
          <?php echo form_input("nama", "", "class='form-control' placeholder='Masukan nama kategori...' required"); ?>
        </div>
        <div class="form-group">
          <?php echo form_label("Status") ?>
          <select name="active" class="form-control">
            <option value="1">Aktif</option>
            <option value="0">Tidak Aktif</option>
          </select>
        </div>
        <?php echo form_submit("Submit", "Simpan", "class='btn btn-success btn-sm'");  ?>
        <?php echo form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="editAdmin">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Tambah Kategori</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?php echo form_open("edit_kategori"); ?>
        <div class="form-group">
          <?php echo form_label("Kategori") ?>
          <?php echo form_hidden("kategori_id", "") ?>
          <?php echo form_input("nama", "", "class='form-control' placeholder='Masukan nama kategori...' required"); ?>
        </div>
        <div class="form-group">
          <?php echo form_label("Status") ?>
          <select name="active" class="form-control">
            <option value="1">Aktif</option>
            <option value="0">Tidak Aktif</option>
          </select>
        </div>
        <?php echo form_submit("Submit", "Simpan", "class='btn btn-success btn-sm'");  ?>
        <?php echo form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
function editAdmin(kategoriId) {
  $.ajax({
    url: "<?php echo base_url("view_kategori?id="); ?>"+kategoriId,
    type: "GET",
    cache: false,
    success: function(result) {
      let hasil = JSON.parse(result);
      $("[name='kategori_id']").val(hasil.data.kategori_id);
      $("[name='nama']").val(hasil.data.nama);
      $("[name='active']").val(hasil.data.active);
    }
  });
}
</script>
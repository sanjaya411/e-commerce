<?php echo $this->session->flashdata('pesan'); ?>
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <button class="btn btn-success btn-sm mb-3" onclick="clearData()" data-toggle="modal" data-target="#addBarang">Tambah Barang</button>
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="data">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Kategori</th>
                <th>Stok</th>
                <th>Berat (Gram)</th>
                <th>Harga</th>
                <th>Foto Cover</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $index = 1;
              foreach ($list_barang as $item) {
                echo "<tr>";
                echo "<td>$index</td>";
                echo "<td>$item->nama</td>";
                echo "<td>$item->nama_kategori</td>";
                echo "<td>$item->stok</td>";
                echo "<td>$item->berat Gram</td>";
                echo "<td>Rp. " . number_format($item->harga, "0", ",", ".") . ",-</td>";
                echo "<td>";
                echo "<img src='" . base_url("assets/img/" . $item->foto) . "' style='width: 100px; height: 100px;'>";
                echo "</td>";
                echo "<td>";
                echo "<button class='btn btn-info btn-sm mx-1' data-toggle='modal' data-target='#editBarang' tooltip='true' title='Edit' onclick=\"editBarang($item->barang_id)\"><i class='fas fa-edit'></i></button>";
                echo "<a href='" . base_url("detail_barang/" . $item->barang_id) . "' class='btn btn-primary btn-sm mx-1' tooltip='true' title='Detail'><i class='fas fa-eye'></i></a>";
                echo "<button class='btn btn-danger btn-sm mx-1' tooltip='true' title='Hapus' onclick=\"deletePrompt('delete_barang/$item->barang_id')\"><i class='fas fa-trash'></i></button>";
                echo "</td>";
                echo "</tr>";
                $index++;
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="addBarang">
  <div class="modal-dialog modal-dialog-scrollable modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Tambah Barang</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?php echo form_open_multipart("add_barang"); ?>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?php echo form_label("Nama Barang"); ?>
              <?php echo form_input("nama", "", "class='form-control' placeholder='Masukan nama barang...' required") ?>
            </div>
            <div class="form-group">
              <?php echo form_label("Kategori"); ?>
              <select name="kategori_id" class="form-control">
                <?php
                foreach ($list_kategori as $item) {
                  echo "<option value='$item->kategori_id'>$item->nama</option>";
                }
                ?>
              </select>
            </div>
            <div class="form-group">
              <?php echo form_label("Stok"); ?>
              <?php echo form_input("stok", "", "class='form-control' placeholder='Masukan stok barang...' required") ?>
            </div>
            <div class="form-group">
              <?php echo form_label("Berat (Gram)"); ?>
              <?php echo form_input(["name" => "berat", "type" => "number"], "", "class='form-control' placeholder='Masukan stok barang...' required") ?>
            </div>
            <div class="form-group">
              <?php echo form_label("Deskripsi"); ?>
              <?php echo form_textarea("description", "", "class='form-control' placeholder='Masukan deskripsi barang...' required"); ?>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <?php echo form_label("Harga"); ?>
              <?php echo form_input("harga", "", "class='form-control' placeholder='Masukan harga barang...' required") ?>
            </div>
            <div class="form-group">
              <img src="<?php echo base_url("assets/img/no_foto.jpg") ?>" style="width: 100%; height: 250px;" id="gambarLoad">
            </div>
            <div class="form-group">
              <?php echo form_label("Foto Cover"); ?>
              <input type="file" name="foto" class="form-control" id="previewGambar" required>
            </div>
          </div>
        </div>
        <?php echo form_submit("Submit", "Simpan", "class='btn btn-success btn-sm'"); ?>
        <?php echo form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="editBarang">
  <div class="modal-dialog modal-dialog-scrollable modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Edit Barang</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?php echo form_open_multipart("edit_barang"); ?>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?php echo form_label("Nama Barang"); ?>
              <?php echo form_hidden('barang_id', ''); ?>
              <?php echo form_input("nama", "", "class='form-control' placeholder='Masukan nama barang...' required") ?>
            </div>
            <div class="form-group">
              <?php echo form_label("Kategori"); ?>
              <select name="kategori_id" id="kategoriEdit" class="form-control">
                <?php
                foreach ($list_kategori as $item) {
                  echo "<option value='$item->kategori_id'>$item->nama</option>";
                }
                ?>
              </select>
            </div>
            <div class="form-group">
              <?php echo form_label("Stok"); ?>
              <?php echo form_input("stok", "", "class='form-control' placeholder='Masukan stok barang...' required") ?>
            </div>
            <div class="form-group">
              <?php echo form_label("Berat (Gram)"); ?>
              <?php echo form_input(["name" => "berat", "type" => "number"], "", "class='form-control' placeholder='Masukan stok barang...' required") ?>
            </div>
            <div class="form-group">
              <?php echo form_label("Deskripsi"); ?>
              <?php echo form_textarea("description", "", "class='form-control' placeholder='Masukan deskripsi barang...' required"); ?>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <?php echo form_label("Harga"); ?>
              <?php echo form_input("harga", "", "class='form-control' placeholder='Masukan harga barang...' required") ?>
            </div>
            <div class="form-group">
              <img style="width: 100%; height: 250px;" id="gambarLoadEdit">
            </div>
            <div class="form-group">
              <?php echo form_label("Foto Cover"); ?>
              <input type="file" name="foto" class="form-control" id="previewGambarEdit">
            </div>
          </div>
        </div>
        <?php echo form_submit("Submit", "Simpan", "class='btn btn-success btn-sm'"); ?>
        <?php echo form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="loading-container d-none">
  <h4>Mohon Tunggu...</h4>
</div>

<script>
  var url = "<?php echo base_url(); ?>";

  function readerImage(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $("#gambarLoad").attr("src", e.target.result);
        $("#gambarLoadEdit").attr("src", e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  function readerImageEdit(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $("#gambarLoadEdit").attr("src", e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#previewGambar").change(function() {
    readerImage(this)
  })
  $("#previewGambarEdit").change(function() {
    readerImageEdit(this)
  })

  function editBarang(barangId) {
    $.ajax({
      url: "<?php echo base_url("barang_edit?id=") ?>" + barangId,
      type: "GET",
      cache: false,
      success: function(result) {
        let hasil = JSON.parse(result);
        $("[name='barang_id']").val(hasil.data.barang_id);
        $("[name='nama']").val(hasil.data.nama);
        $("#kategoriEdit").val(hasil.data.kategori);
        $("[name='berat']").val(hasil.data.berat);
        $("[name='stok']").val(hasil.data.stok);
        $("[name='harga']").val(hasil.data.harga);
        $("[name='description']").val(hasil.data.description);
        $("#gambarLoadEdit").attr("src", url + "assets/img/" + hasil.data.foto);
      }
    });
  }

  function clearData() {
    $("[name='barang_id']").val("");
    $("[name='nama']").val("");
    $("[name='berat']").val("");
    $("[name='stok']").val("");
    $("[name='harga']").val("");
    $("[name='description']").val("");
    $("#gambarLoadEdit").attr("src", "");
  }
</script>
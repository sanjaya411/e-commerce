<!-- Shoping Cart Section Begin -->
<section class="shoping-cart spad">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="shoping__cart__table">
          <table>
            <thead>
              <tr>
                <th class="shoping__product">Products</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Total</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php
              $index = 1;
              $total_berat = 0;
              $total_harga = 0;
              $qty = [];

              if ($this->session->login === TRUE) {
                if (isset($list_cart) && is_array($list_cart) && count($list_cart) > 0) {
                  $total_berat = 0;
                  foreach ($list_cart as $item) {
                    $berat = $item->jumlah_barang * $item->berat;
                    $total_berat += $berat;
                    $harga = $item->jumlah_barang * $item->harga;
                    $total_harga += $harga;

                    echo "<tr>";
                    echo "<input type='hidden' name='barang_id[]' value='" . encrypt_url($item->barang_id) . "'>";
                    echo "<td class='shoping__cart__item'>";
                    echo "<img src='" . base_url("assets/img/" . $item->foto) . "' width='100px'>";
                    echo "<h5>$item->nama</h5>";
                    echo "</td>";
                    echo "<td class='shoping__cart__price'>Rp. " . number_format($item->harga, "0", ",", ".") . ",-</td>";
                    echo "<td class='shoping__cart__quantity'>";
                    echo "<div class='quantity'>";
                    echo "<div class='pro-qty'>";
                    echo "<span class='dec qtybtn' onclick=\"updateCart('" . base_url("update_cart?id=" . encrypt_url($item->barang_id)) . "', $index)\">-</span>";
                    echo "<input name='qty[]' id='qty$index' min='0' max='" . $item->jumlah_barang . "' type='text' value='" . $item->jumlah_barang . "' onkeyup=\"updateCart('" . base_url("update_cart?id=" . encrypt_url($item->barang_id)) . "', $index)\" onblur=\"validateQty('" . base_url("update_cart?id=" . encrypt_url($item->barang_id)) . "', $index)\">";
                    echo "<span class='inc qtybtn' onclick=\"updateCart('" . base_url("update_cart?id=" . encrypt_url($item->barang_id)) . "', $index)\">+</span>";
                    echo "</div>";
                    echo "</div>";
                    echo "</td>";
                    echo "<td class='shoping__cart__total' id='subTotal$index'>Rp. " . number_format($harga, "0", ",", ".") . ",-</td>";
                    echo "<td class='shoping__cart__item__close'>";
                    echo "<a href='" . base_url("delete_cart?id=" . encrypt_url($item->cart_id)) . "' tooltip='true' title='Hapus dari keranjang' class='text-danger icon_close'></a>";
                    echo "</td>";
                    echo "</tr>";
                    array_push($qty, $item->jumlah_barang);
                    $index++;
                  }
                } else {
                  echo "<tr>";
                  echo "<td class='shoping__cart__item text-center' colspan='5'>Tidak ada daftar cart, yuk belanja</td>";
                  echo "</tr>";
                }
              } else {
                // echo "<pre>";
                // print_r($this->cart->contents());
                // echo "</pre>";
                if (count($this->cart->contents()) > 0) {
                  $total_berat = 0;
                  foreach ($this->cart->contents() as $item) {
                    $berat = $item["qty"] * $item["berat"];
                    $total_berat += $berat;
                    $harga = $item["qty"] * $item["price"];
                    $total_harga += $harga;

                    echo "<tr>";
                    echo "<input type='hidden' name='barang_id[]' value='" . encrypt_url($item["id"]) . "'>";
                    echo "<td class='shoping__cart__item'>";
                    echo "<img src='" . base_url("assets/img/" . $item["foto"]) . "' width='100px'>";
                    echo "<h5>".$item["name"]."</h5>";
                    echo "</td>";
                    echo "<td class='shoping__cart__price'>Rp. " . number_format($item["price"], "0", ",", ".") . ",-</td>";
                    echo "<td class='shoping__cart__quantity'>";
                    echo "<div class='quantity'>";
                    echo "<div class='pro-qty'>";
                    echo "<span class='dec qtybtn' onclick=\"updateCart('" . base_url("update_cart?id=" . encrypt_url($item["id"])) . "', $index, '".$item["rowid"]."')\">-</span>";
                    echo "<input name='qty[]' id='qty$index' min='0' max='" . $item["qty"] . "' type='text' value='" . $item["qty"] . "' onkeyup=\"updateCart('" . base_url("update_cart?id=" . encrypt_url($item["id"])) . "', $index, '".$item["rowid"]."')\" onblur=\"validateQty('" . base_url("update_cart?id=" . encrypt_url($item["id"])) . "', $index, '".$item["rowid"]."')\" readonly>";
                    echo "<span class='inc qtybtn' onclick=\"updateCart('" . base_url("update_cart?id=" . encrypt_url($item["id"])) . "', $index, '".$item["rowid"]."')\">+</span>";
                    echo "</div>";
                    echo "</div>";
                    echo "</td>";
                    echo "<td class='shoping__cart__total' id='subTotal$index'>Rp. " . number_format($harga, "0", ",", ".") . ",-</td>";
                    echo "<td class='shoping__cart__item__close'>";
                    echo "<a href='" . base_url("delete_cart?id=" . $item["rowid"]) . "' tooltip='true' title='Hapus dari keranjang' class='text-danger icon_close'></a>";
                    echo "</td>";
                    echo "</tr>";
                    array_push($qty, $item["qty"]);
                    $index++;
                  }
                } else {
                  echo "<tr>";
                  echo "<td class='shoping__cart__item text-center' colspan='5'>Tidak ada daftar cart, yuk belanja</td>";
                  echo "</tr>";
                }
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="shoping__cart__btns">
          <a href="<?= base_url(); ?>" class="primary-btn cart-btn">CONTINUE SHOPPING</a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="shoping__continue">
          <div class="shoping__discount">
            <h5>Discount Codes</h5>
            <form action="#">
              <input type="text" placeholder="Enter your coupon code">
              <button type="submit" class="site-btn">APPLY COUPON</button>
            </form>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="shoping__checkout">
          <h5>Cart Total</h5>
          <ul>
            <li>Total <span id="totalPrice">Rp. <?= number_format($total_harga,"0",",",".") ?></span></li>
          </ul>
          <a href="<?= base_url("checkout"); ?>" class="primary-btn">PROCEED TO CHECKOUT</a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Shoping Cart Section End -->

<script>
  <?php echo "let qty1 = " . json_encode($qty) . ";"; ?>

  function updateCart(link, index, rowId = false) {
    setTimeout(function() {
      var qty = $("#qty" + index).val();
      console.log(qty);
      var number = index - 1;
      var qty2 = qty1[number];
      if (qty == "0") {
        $("#qty" + index).val("1");
        updateCart(link, index, rowId);
      } else {
        $.ajax({
          url: link,
          cache: false,
          type: "POST",
          data: {
            qty: $("#qty" + index).val(),
            qty1: qty2,
            row_id: rowId
          },
          success: function(result) {
            let hasil = JSON.parse(result);
            if (hasil.success !== false) {
              $("#totalPrice").html(hasil.total_harga);
              $("#totalBerat").html(hasil.total_berat + " Gram");
              $("#subTotal" + index).html(hasil.harga);
              $("#berat" + index).html(hasil.berat + " Gram");
              // console.log(hasil.total_berat);
            } else {
              sweet("error", "Gagal", hasil.message);
              $("#qty" + index).val(qty2);
              updateCart(link, index, rowId)
            }
            resultCart();
          }
        });
      }
    }, 500);
  }

  function validateQty(link, index, rowId = false) {
    var qty = $("#qty" + index).val();
    if (qty == "") {
      $("#qty" + index).val("1");
      updateCart(link, index, rowId);
    }
  }
</script>
<!-- Checkout Section Begin -->
<section class="checkout spad" style="margin-top: -70px;">
  <div class="container">
    <div class="checkout__form">
      <h4>Billing Details</h4>
      <?= form_open("create_invoice"); ?>
        <div class="row">
          <div class="col-lg-8 col-md-6">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="">Nama Penerima</label>
                  <input type="text" name="nama_penerima" id="" class="form-control" placeholder="Nama penerima barang" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="">Nomor HP Penerima</label>
                  <input type="number" name="no_hp_penerima" id="" class="form-control" placeholder="Nomor hp penerima barang" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <label for="">Email penerima</label>
                <div class="form-group">
                  <input type="email" class="form-control" name="email_penerima" placeholder="Masukan email penerima" required>
                </div>
              </div>
            </div>
            <h5>Alamat Pengiriman Barang :</h5>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Provinsi</label>
                  <select name="provinsi_id" class="form-control d-block" id="provinsi" value="<?php echo set_value("provinsi_id"); ?>" required>
                    <option selected disabled>-- Pilih Provinsi --</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Kota/Kabupaten</label>
                  <select name="kota_id" class="form-control" id="kota" value="<?php echo set_value("kota_id"); ?>" required>
                    <option selected disabled>-- Pilih Kota/Kabupaten --</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Ekspedisi</label>
                  <select name="expedisi_id" class="form-control" id="expedisi" value="<?php echo set_value("expedisi_id"); ?>" required>
                    <option selected disabled>-- Pilih Expedisi --</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Layanan</label>
                  <select name="expedisi" class="form-control" id="layanan" value="<?php echo set_value("expedisi"); ?>" required>
                    <option selected disabled>-- Pilih Layanan --</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="form-group">
                  <label>Alamat Detail</label>
                  <textarea name="alamat_tujuan" class="form-control" cols="30" rows="5" placeholder="Masukan alamat detail pengiriman" required><?php echo set_value("alamat_tujuan"); ?></textarea>
                  <input type="hidden" name="harga_ongkir" value="">
                  <input type="hidden" name="total_bayar" value="">
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="checkout__order">
              <h4>Your Order</h4>
              <div class="checkout__order__products">Products <span>Total</span></div>
              <ul>
                <?php
                $total_harga = 0;
                if ($this->session->login === TRUE) {
                  foreach ($cart as $item) {
                    $harga = $item->jumlah_barang * $item->harga;
                    $total_harga += $harga;
                    echo "<li>$item->nama <span></span>Rp. " . number_format($harga, "0", ",", ".") . ",-</li>";
                  }
                } else {
                  foreach ($cart as $item) {
                    $total_harga += $item["subtotal"];
                    echo "<li>" . $item["name"] . " <span>Rp. " . number_format($item["subtotal"], "0", ",", ".") . ",-</span></li>";
                  }
                }
                ?>
              </ul>
              <div class="checkout__order__subtotal">Total <span id="totalPrice">Rp. <?= number_format($total_harga, "0", ",", ".") ?>,-</span></div>
              <button type="submit" class="site-btn">PLACE ORDER</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
<!-- Checkout Section End -->

<script>
  <?php
  $total_berat = 0;
  if ($this->session->login !== TRUE) {
    foreach ($this->cart->contents() as $item) {
      $total_berat += $item["berat"] * $item["qty"];
    }
    echo "var totalHarga = " . $this->cart->total() . ";";
  } else {
    foreach ($cart as $item) {
      $total_berat += $item->jumlah_barang * $item->berat;
    }
    echo "var totalHarga = $total_harga;";
  }
  echo "var total_berat = $total_berat;";
  ?>

  $(document).ready(function() {
    $.ajax({
      url: "<?php echo base_url("get_provinsi") ?>",
      type: "POST",
      cache: false,
      success: function(result) {
        $("[name='provinsi_id']").html(result);

        // console.log(result);
      }
    });

    $("#provinsi").change(function() {
      var provinsiId = $("#provinsi").val();
      $.ajax({
        url: "<?php echo base_url("get_city") ?>",
        type: "POST",
        cache: false,
        data: {
          provinsi_id: provinsiId
        },
        success: function(result) {
          $("[name='kota_id']").html(result);
          // console.log(result);
        }
      });
    });

    $("#kota").change(function() {
      $.ajax({
        url: "<?php echo base_url("get_expedisi") ?>",
        type: "POST",
        cache: false,
        success: function(result) {
          $("[name='expedisi_id']").html(result);
          // console.log(result);
        }
      });
    });

    $("#expedisi").change(function() {
      var expedisiName = $("#expedisi").val();
      $.ajax({
        url: "<?php echo base_url("get_services") ?>",
        type: "POST",
        cache: false,
        data: {
          expedisi: expedisiName,
          destination_id: $("#kota").val(),
          berat: total_berat
        },
        success: function(result) {
          let hasil = JSON.parse(result)
          $("#layanan").html(hasil.option);
        }
      });
    });

    $("#layanan").change(function() {
      var expedisi = $("#layanan").val();
      var letak = expedisi.indexOf("z") + 1;
      var text = expedisi.substring(letak);

      var hasil = Number(text) + Number(totalHarga);
      var total = new Intl.NumberFormat('de-DE', {
        style: 'currency',
        currency: 'IDR'
      }).format(hasil)
      letak = total.indexOf(",");
      var total_harga = total.substring(0, letak);

      $("#totalPrice").html(" Rp. " + total_harga + ",-");
      $("[name='harga_ongkir']").val(Number(text));
      $("[name='total_bayar']").val(hasil);
    })
  });
</script>
<!-- Featured Section Begin -->
<section class="featured spad" style="margin-top: -70px;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="section-title">
          <h2>Semua Produk</h2>
        </div>
      </div>
    </div>
    <div class="row featured__filter">
      <?php if (isset($list_barang) && is_array($list_barang) && count($list_barang) > 0) { ?>
        <?php foreach ($list_barang as $item) { ?>
          <div class="col-lg-3 col-md-4 col-sm-6 mix oranges fresh-meat">
            <div class="featured__item">
              <div class="featured__item__pic set-bg" data-setbg="<?= base_url("assets/img/" . $item->foto) ?>">
                <ul class="featured__item__pic__hover">
                  <li><button tooltip="true" title="Add to cart" onclick="<?= "addCart('" . base_url("add_cart?id=" . encrypt_url($item->barang_id)) . "')"; ?>"><i class="fa fa-shopping-cart"></i></button></li>
                  <li><a href="<?php echo base_url("detail_barang?id=" . encrypt_url($item->barang_id)) ?>" tooltip="true" title="Detail"><i class="fa fa-info-circle"></i></a></li>
                </ul>
              </div>
              <div class="featured__item__text">
                <h6><a href="<?php echo base_url("detail_barang?id=" . encrypt_url($item->barang_id)) ?>"><?= $item->nama; ?></a></h6>
                <h5>Rp. <?= number_format($item->harga, "0", ",", "."); ?>,-</h5>
              </div>
            </div>
          </div>
        <?php } ?>
      <?php } else { ?>
        <h3 class="mx-auto text-justify">Maaf untuk saat ini produk tidak tersedia</h3>
      <?php } ?>
    </div>
    <?php if ($pagination > 1) { ?>
      <div class="product__pagination">
        <span class="mr-2">Page</span>
        <?php
        for ($i = 1; $i <= $pagination; $i++) {
          if ($page == $i) {
            echo "<a href='" . base_url("all_barang?page=$i") . "' style='background-color: #7fad39; color: white!important;'>$i</a>";
          } else {
            echo "<a href='" . base_url("all_barang?page=$i") . "'>$i</a>";
          }
        }
        ?>
      </div>
    <?php } ?>
  </div>
</section>
<!-- Featured Section End -->
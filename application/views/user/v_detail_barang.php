<section class="product-details spad">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6">
        <div class="product__details__pic">
          <div class="product__details__pic__item">
            <img class="product__details__pic__item--large" src="<?= base_url("assets/img/" . $barang->foto) ?>" alt="">
          </div>
          <div class="product__details__pic__slider owl-carousel">
            <img data-imgbigurl="<?= base_url("assets/img/" . $barang->foto) ?>" src="<?= base_url("assets/img/" . $barang->foto) ?>" alt="">
            <?php
            foreach ($list_foto as $item) {
              echo "<img data-imgbigurl='" . base_url("assets/img/" . $item->gambar) . "' src='" . base_url("assets/img/" . $item->gambar) . "' alt=''>";
            }
            ?>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6">
        <div class="product__details__text">
          <h3><?= $barang->nama; ?></h3>
          <div class="product__details__price"><?= "Rp. " . number_format($barang->harga, "0", ",", "."); ?></div>
          <button onclick="<?= "addCart('" . base_url("add_cart?id=" . encrypt_url($barang->barang_id)) . "')"; ?>" href="#" class="primary-btn">ADD TO CARD</button>
          <ul>
            <li><b>Stok</b> <span><?= $barang->stok; ?></span></li>
            <li><b>Berat</b> <span><?= $barang->berat ?> Gram</span></li>
          </ul>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="product__details__tab">
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab" aria-selected="true">Description</a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab" aria-selected="false">Information</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab" aria-selected="false">Reviews <span>(1)</span></a>
            </li> -->
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tabs-1" role="tabpanel">
              <div class="product__details__tab__desc">
                <h6>Products Infomation</h6>
                <?= $barang->description; ?>
              </div>
            </div>
            <!-- <div class="tab-pane" id="tabs-2" role="tabpanel">
              <div class="product__details__tab__desc">
                <h6>Products Infomation</h6>
                <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                  Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus.
                  Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam
                  sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo
                  eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat.
                  Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent
                  sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac
                  diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante
                  ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                  Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                  Proin eget tortor risus.</p>
                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem
                  ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit aliquet
                  elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum
                  porta. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus
                  nibh. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.</p>
              </div>
            </div>
            <div class="tab-pane" id="tabs-3" role="tabpanel">
              <div class="product__details__tab__desc">
                <h6>Products Infomation</h6>
                <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                  Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus.
                  Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam
                  sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo
                  eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat.
                  Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent
                  sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac
                  diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante
                  ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                  Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                  Proin eget tortor risus.</p>
              </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
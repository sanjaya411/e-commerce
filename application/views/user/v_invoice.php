<div class="invoice p-3 mb-3" style="margin-top: -50px;">
  <div class="container">
    <div class="card">
      <div class="card-body">
        <!-- title row -->
        <div class="row">
          <div class="col-12">
            <h4>
              <i class="fas fa-shopping-cart"></i> Checkout Invoice
              <small class="float-right">Date: <?php echo date("d F Y"); ?></small>
            </h4>
          </div>
          <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
          <div class="col-sm-4 invoice-col">
            From
            <address>
              <strong><?php echo $pengiriman_dari->nama_toko; ?></strong><br>
              <?php echo $pengiriman_dari->alamat_toko ?><br>
              Phone: <?php echo $pengiriman_dari->no_hp; ?><br>
              Email: <?php echo $pengiriman_dari->email; ?>
            </address>
          </div>
          <!-- /.col -->
          <div class="col-sm-4 invoice-col">
            To
            <address>
              <strong><?php echo $invoice->nama_penerima; ?></strong><br>
              <?php echo $pengiriman->alamat_tujuan; ?><br>
              Phone: <?php echo $invoice->no_hp_penerima; ?><br>
              Email: <?php echo $invoice->email_penerima; ?> <br>
              Kurir: <?php echo $pengiriman->expedisi; ?>
            </address>
          </div>
          <!-- /.col -->
          <div class="col-sm-4 invoice-col">
            <br>
            <br>
            <b>Invoice:</b> #<?php echo $invoice->invoice_number ?><br>
            <b>Payment Due:</b> <?php echo date("d-M-Y H:i", strtotime($invoice->tanggal_jatuh_tempo)); ?><br>
            <b>Status:</b> <?php echo $invoice->invoice_status; ?>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
          <div class="col-12 table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Barang</th>
                  <th>Qty</th>
                  <th>Harga</th>
                  <th>Berat</th>
                  <th>Total Harga</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $total_berat = 0;
                $harga = 0;
                $total_bayar = 0;
                foreach ($list_barang as $item) {
                  $berat = $item->berat * $item->jumlah_barang;
                  $total_berat += $berat;
                  $harga = $item->jumlah_barang * $item->harga;
                  $total_bayar += $harga;
                  echo "<tr>";
                  echo "<td>$item->nama</td>";
                  echo "<td>$item->jumlah_barang</td>";
                  echo "<td>Rp. " . number_format($item->harga, "0", ",", ".") . ",-</td>";
                  echo "<td>$item->berat Gram</td>";
                  echo "<td>Rp. " . number_format($harga, "0", ",", ".") . ",-</td>";
                  echo "</tr>";
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
          <div class="col-sm-6 ml-auto">
            <div class="table-responsive">
              <table class="table">
                <tbody>
                  <tr>
                    <th style="width:50%">Subtotal:</th>
                    <td><?php 
                        echo "Rp. " . number_format($total_bayar, "0", ",", ".") . " ,-"; ?></td>
                  </tr>
                  <tr>
                    <th>Berat</th>
                    <td><?php echo "$total_berat Gram" ?></td>
                  </tr>
                  <tr>
                    <th>Ongkir</th>
                    <td>Rp. <?php $total_bayar += $pengiriman->harga_ongkir;
                            echo number_format($pengiriman->harga_ongkir, "0", ",", "."); ?>,-</td>
                  </tr>
                  <tr>
                    <th>Total Bayar:</th>
                    <td>Rp. <?php echo number_format($total_bayar, "0", ",", "."); ?>,-</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <?php if ($invoice->invoice_status_id == 1) { ?>
          <div class="row">
            <div class="col-12">
              <div class="alert alert-danger">
                <b>Perhatian: </b> Pastikan anda membayar tagihan ini sebelum jatuh tempo. Jika anda belum membayar tagihan dan telah jatuh tempo, maka tagihan & pesanan anda akan dibatalkan secara otomatis
              </div>
            </div>
          </div>
        <?php } ?>
        <div class="row no-print">
          <div class="col-12">
            <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a>
            <?php if ($invoice->invoice_status_id == 1) { ?>
              <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                <i class="fas fa-shopping-cart"></i> Konfirmasi Pemesanan
              </button>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
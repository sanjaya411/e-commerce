<div class="container" style="margin-top: -50px;">
  <div class="row">
    <div class="col card">
      <div class="card-body">
        <?php if (is_array($list_invoice) && count($list_invoice)) { ?>
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th>No</th>
                <th>Nomor Invoice</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
              <?php $index = 1;
              foreach ($list_invoice as $item) {
                echo "<tr>";
                echo "<td>$index</td>";
                echo "<td>$item->invoice_number</td>";
                if ($item->invoice_status_id == 1) {
                  echo "<td><div class='badge badge-primary'>$item->invoice_status</div></td>";
                } elseif ($item->invoice_status_id == 2) {
                  echo "<td><div class='badge badge-success'>$item->invoice_status</div></td>";
                } elseif ($item->invoice_status_id == 3) {
                  echo "<td><div class='badge badge-danger'>$item->invoice_status</div></td>";
                }
                echo "<td><a href='" . base_url("invoice?id=$item->invoice_id&token=" . encrypt_url($item->invoice_id)) . "' class='btn btn-info btn-sm'>Detail</a></td>";
                echo "</tr>";
                $index++;
              } ?>
            </table>
          </div>
        <?php } else { ?>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
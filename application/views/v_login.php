<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url("assets/") ?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url("assets/") ?>dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Sweet -->
  <script src="<?= base_url("assets/") ?>plugins/sweetalert2/sweetalert2.all.min.js"></script>
  <script>
    function sweet(icon, title, text) {
      Swal.fire({
        icon: icon,
        title: title,
        text: text
      })
    }
  </script>
</head>

<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="<?= base_url("assets/") ?>index2.html"><b>SansShop</b> Login</a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <?= $this->session->flashdata('pesan'); ?>
        <?= form_open("auth") ?>
        <?= form_error("username", "<small class='text-danger'>", "</small>"); ?>
        <div class="input-group mb-3">
          <input type="text" name="username" class="form-control" placeholder="Username" autocomplete="off" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <?= form_error("password", "<small class='text-danger'>", "</small>"); ?>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-6">
            <a href="<?php echo base_url("home"); ?>" class="btn btn-success">Kembali</a>
          </div>
          <!-- /.col -->
          <div class="col-6">
            <button type="submit" class="btn btn-primary btn-block">Login</button>
          </div>
          <!-- /.col -->
        </div>
        <?= form_close(); ?>
      </div>
      <!-- /.login-card-body -->
      <div class="card-footer">
        <p>Belum punya akun? <a href="<?php echo base_url("register"); ?>">Register disini!</a></p>
      </div>
    </div>
  </div>
  <!-- /.login-box -->

  <!-- jQuery -->
  <script src="<?= base_url("assets/") ?>js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="<?= base_url("assets/") ?>js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url("assets/") ?>js/adminlte.min.js"></script>
</body>

</html>